# Start of translating the kraken output to taxonomical classification
echo " "
echo "------------Taxonomical classification process starts now.....(processing)-----------------------------"
export PATH=/isilon/biodiversity/users/prabr/hari/kraken/scripts/kraken-translate:$PATH
#/isilon/biodiversity/users/prabr/hari/kraken/scripts/kraken-translate --db /isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/Utilities/DB/kraken/ /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Kraken_Output/Kraken_Contig_Output.hits > /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Kraken_Output/Kraken_Taxonomical_Output.labels
/isilon/biodiversity/users/prabr/hari/kraken/scripts/kraken-translate --mpa-format --db /isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/Utilities/DB/kraken/ /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Kraken_Output/Kraken_Contig_Output.hits > /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Kraken_Output/Kraken_Taxonomical_Output_With_Rank.labels

echo " "
echo "------------Taxonomical classification process successfully completed----------------------------------"
# End of translating the kraken output to taxonomical classification

