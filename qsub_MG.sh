#!/bin/bash/
#$ -pe smp 20
#$ -q all.q@biocomp-0-12
#$ -cwd
#$ -S /bin/bash
#$ -N S002994_contigs_BLAST_nt_Qsub
#$ -M Ramanandan.Prabhakaran@AGR.GC.CA

bash JSN_Taxonomical_Classification_pipeline_Version2.sh
