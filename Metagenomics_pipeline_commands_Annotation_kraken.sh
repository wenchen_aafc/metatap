#/bin/sh
#!/usr/bash
# Start of annotation step of reads

mkdir /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Kraken_Output/
echo " "
echo "------------Annotation of contigs based on kraken database starts now....(processing)---------------------"
echo " "
export PATH=/isilon/biodiversity/users/prabr/hari/kraken/:$PATH
cd /isilon/biodiversity/users/prabr/hari/kraken/
kraken --db /isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/Utilities/DB/kraken --quick --preload --min-hits 5 --threads 20 --output /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Kraken_Output/Kraken_Contig_Output.hits /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/metAMOS_Output/proba.asm.contig.fa
cd ..
pwd
echo " "
echo "------------Annotation of contigs based on kraken database successfully completed------------------------"
# End of annotation step of reads
