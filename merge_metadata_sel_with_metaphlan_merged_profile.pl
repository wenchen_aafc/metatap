#!/usr/bin/env perl

use strict;
use warnings;

use Bio::SeqIO;
use File::Basename;
use File::Copy;
#use File::spec;
use Scalar::Util qw/reftype/;
use Set::Scalar;
use List::MoreUtils qw(firstidx);
use Cwd;

my $profile=shift or die "merged profile?";
my @suffix=(".txt", ".tab", ".tabular", ".tsv", ".csv");
my $abs_path = File::Spec->rel2abs($profile);
my $ext = (fileparse("$abs_path", qr/\.[^.]*/))[2];
#my $dir= (fileparse("$abs_path", qr/\.[^.]*/))[1];
##print $abs_path, "\n", $dir, "\n\n", $ext, "\n";

# get current directory
my $dir = getcwd;

my $basename = basename($abs_path, @suffix);
#print $basename, "\t", "$basename.tab", "\n";

my $meta=shift or die "Please provide metadata in csv or tab format";
my $features=shift or die "selected columns (features) no more than 3 excluding the sampleID, e.g. Crop,Treat,Rep";
my $sample=shift or die "Please provide the header name of the sampleIDs";

my @fts_all; 
my @fts;
my $ft;
@fts_all = split /,/, $features;
if (@fts_all) {
  #print @fts_all, "\n";
  if ( scalar @fts_all > 3 ) {   
    @fts_all=@fts_all[0..2];
    print "\n\nMore than 3 features had been selected from the $meta!\nWill only keep the first 3: ", join(", ", @fts_all), "!", "\nOr you can select different features and re-run this script\n\n";
    unshift @fts_all, $sample;
    @fts=@fts_all;
    #print @fts, "\n";
  } elsif (scalar @fts_all >1 ) {
    print "Selected ", scalar @fts_all, " features: ", join(", ", @fts_all), "!", " you can select different features and re-run this script\n\n";
    unshift @fts_all, $sample;
    @fts=@fts_all;
    print @fts, "\n";
  } else { 
    $ft=$features;
    print "Selected 1 feature: ", $features, "!", " you can select up to 3 features and re-run this script\n\n";
    @fts = ($sample, $ft);
    print @fts, "\n";
  }
}
 
@fts = do { my %seen; grep { !$seen{$_}++ } @fts };
print join(", ", @fts), "\n";
#my $max;
#my $max = (sort { $b <=> $a } @fts)[0];
##print $max, "\n";


my $h_meta={};
# skip to the header
my $samp;
my @treats;
my $treat;
my @header;
my $index_samp;
my $index;
my @indice;
my $ct=0;
# must open file agaSet::Scalarin, as previous loop has done
# watch out the encoding!
#open (my $fh, '<:utf8', "<$meta")  or die "Can't open $meta: $!";
open (my $fh1, "<$meta")  or die "Can't open $meta: $!";
while (<$fh1>) {
  chomp $_;
  $ct++;
  my @cols=split /[,\t]+/, $_;
  if ( $ct == 1) {
    if ( grep {$_ eq $sample} @cols )  {
      @header=@cols;
      print @header, "\n";
      #print $_, "\n";
      ##print join(",", @cols_all), "\n";
      if (@fts) {
        print @fts, "\n";
        unless ( isSubset(\@fts, \@header) ) {
          die "Not all selected features in $meta, Please check $meta and re-run the script\n\n";
        } 
        foreach my $tr (@fts) {
          #print $tr; 
          #$index= grep { $header[$_] eq $tr } 0..$#header;
          $index=firstidx { $_ eq $tr } @header;
          push (@indice, $index);
          if ( $tr eq $sample) {
            #print $tr, "\t";
            #$index_samp= grep { $header[$_] eq $sample } 0..$#header;
            $index_samp=firstidx { $_ eq $sample } @header;
            #print $index_samp, "\t";
          }
        }
      }
    } else {
      die "The header of $meta must contain $sample as sampleID\n\n";
    } 
  } 
}
close $fh1;

print $index_samp, "\t", join(",", @indice), "\n";

# open file again;
open (my $fh, "<$meta")  or die "Can't open $meta: $!";
my $count=0;
while (<$fh>) {
  chomp $_;
  $count++;
  my @cols_all =split /[,\t]+/, $_;
  if ( ($count == 1) && ( grep {$_ eq $sample} @cols_all) )  {
    next;
  } else {
    foreach my $ind (@indice) {
      chomp $ind;
      if ((defined $ind) && ($ind <= $#cols_all) ) {
        $h_meta->{$cols_all[$index_samp]}->{$header[$ind]}=$cols_all[$ind];
        print $cols_all[$index_samp], "\t", $cols_all[$index], "\n";
     }
    #} elsif (defined $treat) {
    #  $h_meta->{$cols[0]}->{$treat}=$cols[1];
    }
  }
}
close $fh;

my $sel=$features;
$sel=~ s/,/_/g;
foreach my $k (sort keys $h_meta) {
  if (@indice) {
    foreach (@indice) {
      chomp $_;
      print $k, "\t", $h_meta->{$k}->{$header[$_]}, "\t";
    }
    print "\n";
  }
}


open (F, "<$profile");
open (OUT, ">$dir/$basename.withMeta.$sel.tab");
my $count1=0;
while (<F>) {
  chomp $_;
  $count1++;
  if ( /^# Constructed from biom file/ ) {
    next;
  } elsif ( /^ID\t/ || /^#OTU ID/ ) {
    ##print $_, "\n\n";
    ##print OUT $_, "\n";
    my @cols = split /\t/, $_;
    @cols = map {$_ =~ s/\_.*//; $_} @cols;
    ##print join ("\t", @cols), "\n";
    if (@indice) {
      foreach my $ind (@indice) {
        chomp $ind;
        print $ind, "\t", $header[$ind], "\n";
        my @m;
        push (@m, $header[$ind]);
        foreach my $s (@cols) {
          chomp $s;
          if (exists $h_meta->{$s}) {
            push (@m, $h_meta->{$s}->{$header[$ind]});
          }
        }
        print join ("\t", @m), "\n";
        print OUT join ("\t", @m), "\n";
      }
    }
  } else {
    print OUT $_, "\n";
  }
}

close F;
close OUT;
print "\nThe merged $meta and $profile has been saved to $dir/$basename.withMeta.$sel.tab\n\n";

sub isSubset {
  my ($littleSet, $bigSet) = @_;
  foreach (@{$littleSet}) {
    return 0 unless ($_ ~~ @{$bigSet});
  }
  return 1;
}

