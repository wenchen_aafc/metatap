#/bin/sh
#!/usr/bash

mkdir Trimmomatic_Analysis/FastQC_Report
cd Trimmomatic_Analysis/FastQC_Report
fastqc /isilon/biodiversity/users/prabr/hari/Trimmomatic_Analysis/output_forward_paired.fq.gz --outdir=/isilon/biodiversity/users/prabr/hari/Trimmomatic_Analysis/FastQC_Report
fastqc /isilon/biodiversity/users/prabr/hari/Trimmomatic_Analysis/output_reverse_paired.fq.gz --outdir=/isilon/biodiversity/users/prabr/hari/Trimmomatic_Analysis/FastQC_Report
cd ..
export PATH=/isilon/biodiversity/users/prabr/hari/fastq_screen_v0.4.4/:$PATH
mkdir Trimmomatic_Analysis/Fastq_Screen_Output
perl fastq_screen --threads 8 --nohits --aligner bowtie2 --bowtie2 "--local" --conf /isilon/biodiversity/users/prabr/hari/fastq_screen_v0.4.4/fastq_screen.conf --paired /isilon/biodiversity/users/prabr/hari/Trimmomatic_Analysis/output_forward_paired.fq.gz /isilon/biodiversity/users/prabr/hari/Trimmomatic_Analysis/output_reverse_paired.fq.gz --outdir /isilon/biodiversity/users/prabr/hari/Trimmomatic_Analysis/fastq_screen_output


