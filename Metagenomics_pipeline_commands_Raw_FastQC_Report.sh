#/bin/sh
#!/usr/bash

# Start of quality check of raw reads
echo " "
echo "-----------Quality check of raw reads starts now....(processing)----------"
mkdir /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis1/Raw_FastQC_Report
mkdir /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis1/Raw_FastQC_Report/Logs
cd /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis1/Raw_FastQC_Report
fastqc /isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/JSNsamples/S002994_r1.fastq --outdir=/isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis1/Raw_FastQC_Report > /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis1/Raw_FastQC_Report/Logs/S002994_r1_fastqc.logs 2>&1
fastqc /isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/JSNsamples/S002994_r2.fastq --outdir=/isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis1/Raw_FastQC_Report > /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis1/Raw_FastQC_Report/Logs/S02994_r1_fastqc.logs 2>&1
cd /isilon/biodiversity/users/prabr/hari/
echo "Present working directory is "pwd
echo " "
echo "-----------Quality check of raw reads successfully completed--------------"
# End of quality check of raw reads
