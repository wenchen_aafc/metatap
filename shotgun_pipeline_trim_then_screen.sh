#! /bin/bash

#all tools: https://bitbucket.org/biobakery/biobakery/wiki/Home
#wget https://cran.r-project.org/src/contrib/infotheo_1.2.0.tar.gz
# sudo R CMD INSTALL --build infotheo_1.2.0.tar.gz 
#  sudo R CMD INSTALL --build ccrepe_1.1.1.tar.gz 
# for installing dependencies, check file shotgun_pipeline.sh

################################################
# index wheat genome (or other genomes for screening purposes
################################################
# cd ~/Desktop/Data/wheat_genome
# time bash ~/bowtie2-2.2.6/make_wheatgenome1to7ABD_index_use_fasta.sh

################################################
# check commands location
################################################
metaphlan_dir='/home/wenchenaafc/metaphlan'; 
[ ! -d "$metaphlan_dir" ] && echo "metaphlan direcotry doesn't exist" && exit 1;
metaphlan_script=$metaphlan_dir/metaphlan.py; 
[ ! -f "$metaphlan_script" ] && echo "path to metaphlan.py doesn't exist" && exit 1;
metaphlan_db=$metaphlan_dir/bowtie2db/mpa;
metaphlan_merge=$metaphlan_dir/utils/merge_metaphlan_tables.py;
[ ! -f "$metaphlan_merge" ] && echo "path to merge_metaphlan_tables.py doesn't exist" && exit 1;
lefse_dir='/home/wenchenaafc/lefse';
[ ! -d "$lefse_dir" ] && echo "lefse directory doesn't exist" && exit 1;
microbiome_helper_dir='/home/wenchenaafc/microbiome_helper';
[ ! -d "$microbiome_helper_dir" ] && echo "microbiome directory doesn't exist" && exit 1;


################################################
# setup input output and metadata locations
################################################
# tar xvf 140520_7001410_0133_BC4CPDACXX_lane4.tar | mv samples lane4;
# tar xvf 140520_7001410_0133_BC4CPDACXX_lane5.tar | mv samples lane5;

input_dir="/media/wenchenaafc/Data/140520_7001410_0133_BC4CPDACXX/samples";
input_meta="/home/wenchenaafc/Data/140520_7001410_0133_BC4CPDACXX/meta.tab";
output_dir="/media/wenchenaafc/Data/140520_7001410_0133_BC4CPDACXX/samples_output"


################################################
# check if output dir exists and empty,
################################################
if [ -d $output_dir ]; then
    # the directory exists
    [ "$(ls -A $output_dir)" ] && echo "$output_dir is not Empty" && echo "will save output to $output_dir.0" && mkdir $output_dir\_0 && output_dir=$output_dir\_0 || echo "$output_dir is empty"; output_dir=$output_dir;
  else 
    mkdir $output_dir;
fi

echo $output_dir;


################################################
# get sample list
################################################
[ -f "$input_dir/sample.list" ] && rm $input_dir/sample.list;
ls $input_dir/*fastq.gz|while read line; do echo $line; full=$(basename $line); base=${full%_r*}; echo $full; echo $base >> temp; done; sort temp|uniq >> $input_dir/sample.list; rm temp; 


################################################
# Go to output folder
################################################
cd $output_dir;

################################################
## trim raw reads using Trimmomatic
################################################
mkdir trimmed;
for i in $(echo $(sed "s/\n/ /g" $input_dir/sample.list)); 
do 
  echo $i;
  time java -jar ~/Trimmomatic-0.35/trimmomatic-0.35.jar PE -threads 12 -trimlog trimmed/$i.trimLog -phred33 $input_dir/$i\_r1.fastq.gz $input_dir/$i\_r2.fastq.gz trimmed/$i\_r1.paired.fastq.gz trimmed/$i\_r1.unpaired.fastq.gz trimmed/$i\_r2.paired.fastq.gz trimmed/$i\_r2.unpaired.fastq.gz LEADING:25 TRAILING:25 SLIDINGWINDOW:1:25 MINLEN:50 AVGQUAL:25;
  # 73 min each using 12 threads
done;

################################################
## screen unmatched sequence using bowtie
### run bowtie2 to screen out wheat seqs
################################################
mkdir screened_unmatched
for i in $(echo $(sed "s/\n/ /g" $input_dir/sample.list)); 
do 
  echo $i;
  #### write pairs that didn't align concordantly to genome
  time bowtie2 -x /home/wenchenaafc/Data/wheat_genome/wg1to7ABD -p 12 -1 trimmed/$i\_r1.paired.fastq.gz -2 trimmed/$i\_r2.paired.fastq.gz --un-conc-gz screened_unmatched/$i.fastq.gz >> /dev/null; mv screened_unmatched/$i.fastq.1.gz screened_unmatched/$i\_r1.paired.screen.fastq.gz; mv screened_unmatched/$i.fastq.2.gz screened_unmatched/$i\_r2.paired.screen.fastq.gz;
  #### write unpaired reads that didn't align to genome
  time bowtie2 -x /home/wenchenaafc/Data/wheat_genome/wg1to7ABD -p 12 -1 trimmed/$i\_r1.unpaired.fastq.gz -2 trimmed/$i\_r2.unpaired.fastq.gz --un-gz screened_unmatched/$i.fastq.gz >> /dev/null; mv screened_unmatched/$i.fastq.1.gz screened_unmatched/$i\_r1.unpaired.screen.fastq.gz; mv screened_unmatched/$i.fastq.2.gz screened_unmatched/$i\_r2.unpaired.screen.fastq.gz;
done;

################################################
## screen matched sequence using bowtie
### run bowtie2 to screen sequences matched wheat seqs
################################################
mkdir screened_matched
for i in $(echo $(sed "s/\n/ /g" $input_dir/sample.list)); 
do 
  echo $i;
  #### write pairs that align concordantly to genome
  time bowtie2 -x /home/wenchenaafc/Data/wheat_genome/wg1to7ABD -p 12 -1 trimmed/$i\_r1.paired.fastq.gz -2 trimmed/$i\_r2.paired.fastq.gz --al-conc-gz screened_matched/$i.fastq.gz >> /dev/null; mv screened_matched/$i.fastq.1.gz screened_matched/$i\_r1.paired.screen.fastq.gz; mv screened_matched/$i.fastq.2.gz screened_matched/$i\_r2.paired.screen.fastq.gz;
  #### write unpaired reads that align to genome
  time bowtie2 -x /home/wenchenaafc/Data/wheat_genome/wg1to7ABD -p 12 -1 trimmed/$i\_r1.unpaired.fastq.gz -2 trimmed/$i\_r2.unpaired.fastq.gz --al-gz screened_matched/$i.fastq.gz >> /dev/null; mv screened_matched/$i.fastq.1.gz screened_matched/$i\_r1.unpaired.screen.fastq.gz; mv screened_matched/$i.fastq.2.gz screened_matched/$i\_r2.unpaired.screen.fastq.gz;
done;

#sample="S00
#pair="r1 r2"; for i in $pair; do echo S002986_${i}.screen.fastq.bz2; bzip2 -dk S002986_${i}.screen.fastq.bz2; gzip -1kf S002986\_${i}.screen.fastq; done
#tar -cvjSf test.tar.bz2 *.screen.fastq.gz # 3.3G no advantage in saving space
#tar -pczf test.tar.gz *.screen.fastq.gz # 3.3G no advantage in saving space

################################################
## metaphlan
################################################
mkdir profiled_samples;
for i in $(echo $(sed "s/\n/ /g" $input_dir/sample.list)); 
do 
  echo $i;
  # non-local presets did not classify anything
  #gunzip screened/${i}\_[rR][1-2]*.*paired.screen.fastq.gz --stdout | $metaphlan_script --bowtie2db $metaphlan_dir/bowtie2db/mpa --bt2_ps sensitive --input_type multifastq --bowtie2out ${i}\_s.bt2out > profiled_samples/${i}\_s.txt
  #gunzip screened/${i}\_[rR][1-2]*.*paired.screen.fastq.gz --stdout | $metaphlan_script --bowtie2db $metaphlan_dir/bowtie2db/mpa --bt2_ps very-sensitive --input_type multifastq --bowtie2out ${i}\_vs.bt2out > profiled_samples/${i}\_vs.txt
  gunzip screened/${i}\_[rR][1-2]*.*paired.screen.fastq.gz --stdout | $metaphlan_script --bowtie2db $metaphlan_dir/bowtie2db/mpa --bt2_ps sensitive-local --input_type multifastq --bowtie2out ${i}\_sl.bt2out > profiled_samples/${i}\_sl.txt
  gunzip screened/${i}\_[rR][1-2]*.*paired.screen.fastq.gz --stdout | $metaphlan_script --bowtie2db $metaphlan_dir/bowtie2db/mpa --bt2_ps very-sensitive-local --input_type multifastq --bowtie2out ${i}\_vsl.bt2out > profiled_samples/${i}\_vsl.txt
done

################################################
## humann: function /pathway analyses
################################################
# make diamond db
# diamond makedb --in kegg.reduced.fasta -d kegg.reduced
# blastdb
#perl ~/microbiome_helper/run_pre_humann.pl -p 4 -o pre_humann/ screened/S00298C_r1.screen.fastq.gz
#diamond blastx -q screened/S002987_r1.screen.unpaired.fastq.gz --db ~/humann/data/diamond_db/kegg.reduced  -a tbd.faa -o tbd

## run blastx using diamond
[ -d "humann_profiling_unmatch" ] && rm -fr "humann_profiling_unmatch"
[ -d "humann_profiling_unmatch" ] && rm -fr "humann_profiling_unmatch"
mkdir humann_profiling_unmatch;
mkdir humann_profiling_match;
[ -d "pre_humann_unmatch" ] && rm -fr "pre_humann_unmatch";
for i in $(echo $(sed "s/\n/ /g" $input_dir/sample.list)); 
do 
  echo $i;
  #time gunzip screened/${i}\_r*.screen.*.fastq.gz --stdout |$microbiome_helper_dir/run_pre_humann.pl -p 12 -o pre_humann/ $1
  time $microbiome_helper_dir/run_pre_humann.pl -p 16 -o pre_humann_unmatch/ screened_unmatched/${i}\_[rR][1-2]*.*paired.screen.fastq.gz
done;

[ -d "pre_humann_match" ] && rm -fr "pre_humann_match"
for i in $(echo $(sed "s/\n/ /g" $input_dir/sample.list)); 
do 
  echo $i;  
  time $microbiome_helper_dir/run_pre_humann.pl -p 16 -o pre_humann_match/ screened_matched/${i}\_[rR][1-2]*.*paired.screen.fastq.gz
done;



mkdir output
#The resulting table contains relative abundances with microbial clades as rows and samples as columns.
$metaphlan_dir/utils/merge_metaphlan_tables.py $(ls profiled_samples/*l.txt|sort) > output/merged_abundance_table_trimmed.txt


mkdir output_images
# missing python modules
# sudo apt-get purge scipy
# sudo pip install scipy
#The metaphlan_hclust_heatmap.py script in the MetaPhlAn plotting_scripts folder can now be used to perform hierarchical clustering of both samples and clades to generate the heatmap:
$metaphlan_dir/plotting_scripts/metaphlan_hclust_heatmap.py -c bbcry --top 25 --minv 0.1 -s log -m average -d braycurtis -f correlation --in output/merged_abundance_table_trimmed.txt --out output_images/abundance_heatmap_trimmed.png


mkdir -p tmp
#display output_images/*.png
# graphlan for merged 
$metaphlan_dir/plotting_scripts/metaphlan2graphlan.py output/merged_abundance_table_trimmed.txt  --tree_file tmp/merged.tree_trimmed.txt --annot_file tmp/merged.annot_trimmed.txt
graphlan_annotate.py --annot tmp/merged.annot_trimmed.txt tmp/merged.tree_trimmed.txt tmp/merged_trimmed.xml
graphlan.py --dpi 200 tmp/merged_trimmed.xml output_images/merged_trimmed.png


#############
### biomarker
##############

# in this script we show how to perform the biomarker discovery operation
# using LEfSe. The scripts require LEfSe to be installed and in the system path

# convert the sample names in the table of abundance into classes (i.e. the two bodysites)
##sed 's/\([A-Z][A-Z]\)_\w*/\1/g' output/merged_abundance_table.txt > tmp/merged_abundance_table.4lefse.txt

# merge merged profile with metadata
#perl ../merge_metadata_with_metaphlan_merged_profile.pl output/merged_abundance_table_untrimmed.txt ../meta.csv 3
#perl ../merge_metadata_with_metaphlan_merged_profile.pl output/merged_abundance_table_trimmed.txt ../meta.csv 3

# Crop, Treat, Sample
perl ../merge_metadata_sel_with_metaphlan_merged_profile.pl output/merged_abundance_table_untrimmed.txt ../meta.csv Crop,Treat,Sample Sample

perl ../merge_metadata_sel_with_metaphlan_merged_profile.pl output/merged_abundance_table_trimmed.txt ../meta.csv Crop,Treat,Sample Sample

# Crop, Sample
perl ../merge_metadata_sel_with_metaphlan_merged_profile.pl output/merged_abundance_table_untrimmed.txt ../meta.csv Crop Sample

perl ../merge_metadata_sel_with_metaphlan_merged_profile.pl output/merged_abundance_table_trimmed.txt ../meta.csv Crop Sample

# Treat, Sample
perl ../merge_metadata_sel_with_metaphlan_merged_profile.pl output/merged_abundance_table_untrimmed.txt ../meta.csv Treat Sample

perl ../merge_metadata_sel_with_metaphlan_merged_profile.pl output/merged_abundance_table_trimmed.txt ../meta.csv Treat Sample


# first LEfSe step: format the input specifying that the class info is in the first row
# Crop, Treat, Sample
$lefse_dir/format_input.py output/merged_abundance_table_trimmed.withMeta.Crop_Treat_Sample.tab tmp/trimmed.withMeta.Crop_Treat_Sample.lefs -c 3 -s 2 -u 1 -o 1000000

$lefse_dir/format_input.py output/merged_abundance_table_untrimmed.withMeta.Crop_Treat_Sample.tab tmp/untrimmed.withMeta.Crop_Treat_Sample.lefs -c 3 -s 2 -u 1 -o 1000000

# Crop, Sample
$lefse_dir/format_input.py output/merged_abundance_table_trimmed.withMeta.Crop.tab tmp/trimmed.withMeta.Crop.lefs -c 2 -u 1 -o 1000000

$lefse_dir/format_input.py output/merged_abundance_table_untrimmed.withMeta.Crop.tab tmp/untrimmed.withMeta.Crop.lefs -c 2 -u 1 -o 1000000


# Treat, Sample
$lefse_dir/format_input.py output/merged_abundance_table_trimmed.withMeta.Treat.tab tmp/trimmed.withMeta.Treat.lefs -c 2 -u 1 -o 1000000

$lefse_dir/format_input.py output/merged_abundance_table_untrimmed.withMeta.Treat.tab tmp/untrimmed.withMeta.Treat.lefs -c 2 -u 1 -o 1000000

# run the LEfSe biomarker discovery tool with default options apart for the 
# threshold on the LDA effect size which is increaset to 4
# Crop, Treat, Sample
#$lefse_dir/run_lefse.py tmp/trimmed.withMeta.Crop_Treat_Sample.lefs tmp/trimmed.withMeta.Crop_Treat_Sample.lefs.out -l 2

#$lefse_dir/run_lefse.py tmp/untrimmed.withMeta.Crop_Treat_Sample.lefs tmp/untrimmed.withMeta.Crop_Treat_Sample.lefs.out -l 2 -y 1


$lefse_dir/run_lefse.py tmp/trimmed.withMeta.Crop_Treat_Sample.lefs tmp/trimmed.withMeta.Crop_Treat_Sample.lefs.out -l 2 -y 1 -a 0.01 -w 0.01

$lefse_dir/run_lefse.py tmp/untrimmed.withMeta.Crop_Treat_Sample.lefs tmp/untrimmed.withMeta.Crop_Treat_Sample.lefs.out -l 2 -y 1 -a 0.01 -w 0.01


# Crop, Sample
$lefse_dir/run_lefse.py tmp/trimmed.withMeta.Crop.lefs tmp/trimmed.withMeta.Crop.lefs.out -l 2

$lefse_dir/run_lefse.py tmp/untrimmed.withMeta.Crop.lefs tmp/untrimmed.withMeta.Crop.lefs.out -l 2 -y 1

# Treat, Sample
$lefse_dir/run_lefse.py tmp/trimmed.withMeta.Treat.lefs tmp/trimmed.withMeta.Treat.lefs.out -l 2

$lefse_dir/run_lefse.py tmp/untrimmed.withMeta.Treat.lefs tmp/untrimmed.withMeta.Treat.lefs.out -l 2 -y 1


# Plot the resulting list of biomarkers with the corresponsing effect size
# Crop, Treat, Sample
$lefse_dir/plot_res.py --dpi 300 tmp/trimmed.withMeta.Crop_Treat_Sample.lefs.out output_images/trimmed.withMeta.Crop_Treat_Sample.lefs_biomarkers.png

$lefse_dir/plot_res.py --dpi 300 tmp/untrimmed.withMeta.Crop_Treat_Sample.lefs.out output_images/untrimmed.withMeta.Crop_Treat_Sample.lefs_biomarkers.png

# Crop,Sample
$lefse_dir/plot_res.py --dpi 300 tmp/trimmed.withMeta.Crop.lefs.out output_images/trimmed.withMeta.Crop.lefs_biomarkers.png

$lefse_dir/plot_res.py --dpi 300 tmp/untrimmed.withMeta.Crop.lefs.out output_images/untrimmed.withMeta.Crop.lefs_biomarkers.png

# Treat, Sample
$lefse_dir/plot_res.py --dpi 300 tmp/trimmed.withMeta.Treat.lefs.out output_images/trimmed.withMeta.Treat.lefs_biomarkers.png

$lefse_dir/plot_res.py --dpi 300 tmp/untrimmed.withMeta.Treat.lefs.out output_images/untrimmed.withMeta.Treat.lefs_biomarkers.png

# Plot the biomarkers on the underlying cladogram
# Crop, Treat, Sample
$lefse_dir/plot_cladogram.py --dpi 300 --format png tmp/trimmed.withMeta.Crop_Treat_Sample.lefs.out output_images/trimmed.withMeta.Crop_Treat_Sample_lefse_biomarkers_cladogram.png 

$lefse_dir/plot_cladogram.py --dpi 300 --format png tmp/untrimmed.withMeta.Crop_Treat_Sample.lefs.out output_images/untrimmed.withMeta.Crop_Treat_Sample_lefse_biomarkers_cladogram.png 

# Crop, Sample
$lefse_dir/plot_cladogram.py --dpi 300 --format png tmp/trimmed.withMeta.Crop.lefs.out output_images/trimmed.withMeta.Crop_lefse_biomarkers_cladogram.png 

$lefse_dir/plot_cladogram.py --dpi 300 --format png tmp/untrimmed.withMeta.Crop.lefs.out output_images/untrimmed.withMeta.Crop_lefse_biomarkers_cladogram.png 

# Treat, Sample
$lefse_dir/plot_cladogram.py --dpi 300 --format png tmp/trimmed.withMeta.Treat.lefs.out output_images/trimmed.withMeta.Treat_lefse_biomarkers_cladogram.png 

$lefse_dir/plot_cladogram.py --dpi 300 --format png tmp/untrimmed.withMeta.Treat.lefs.out output_images/untrimmed.withMeta.Treat_lefse_biomarkers_cladogram.png 


# Plot one features specifically (Firmicutes in this case) 
#echo "one:two:three:four:five" | awk -F: '{ st = index($0,":");print $1 "  " substr($0,st+1)}'

grep "H2O" tmp/untrimmed.withMeta.Crop_Treat_Sample.lefs.out > tbd; cut -f1 tbd> tbd1; cat tbd1|while read line; do echo $line; tax=${line##*__}; echo $tax; $lefse_dir/plot_features.py -f one --feature_name "$line" tmp/untrimmed.withMeta.Crop_Treat_Sample.lefs tmp/untrimmed.withMeta.Crop_Treat_Sample.lefs.out output_images/untrimmed.withMeta.Crop_Treat_Sample_$tax.png; done; rm tbd; rm tbd1; 

grep "H2O" tmp/trimmed.withMeta.Crop_Treat_Sample.lefs.out > tbd; cut -f1 tbd> tbd1; cat tbd1|while read line; do echo $line; tax=${line##*__}; echo $tax; $lefse_dir/plot_features.py -f one --feature_name "$line" tmp/trimmed.withMeta.Crop_Treat_Sample.lefs tmp/trimmed.withMeta.Crop_Treat_Sample.lefs.out output_images/trimmed.withMeta.Crop_Treat_Sample_$tax.png; done; rm tbd; rm tbd1; 
  
#grep "H2O" tmp/untrimmed.withMeta.Crop.lefs.out > tbd; cut -f1 tbd> tbd1; cat tbd1|while read line; do echo $line; tax=${line##*__}; echo $tax; $lefse_dir/plot_features.py -f one --feature_name "$line" tmp/untrimmed.withMeta.Crop.lefs tmp/untrimmed.withMeta.Crop_Treat_Sample.lefs.out output_images/untrimmed.withMeta.Crop_$tax.png; done; rm tbd; rm tbd1; 
 
#grep "H2O" tmp/untrimmed.withMeta.Treat.lefs.out > tbd; cut -f1 tbd> tbd1; cat tbd1|while read line; do echo $line; tax=${line##*__}; echo $tax; $lefse_dir/plot_features.py -f one --feature_name "$line" tmp/untrimmed.withMeta.Treat.lefs tmp/untrimmed.withMeta.Treat_Treat_Sample.lefs.out output_images/untrimmed.withMeta.Treat_$tax.png; done; rm tbd; rm tbd1; 


#$lefse_dir/plot_features.py -f one --feature_name "k__Bacteria.p__Proteobacteria.c__Alphaproteobacteria.o__Rhizobiales" tmp/untrimmed.withMeta.Treat.lefs tmp/untrimmed.withMeta.Treat.lefs.out output_images/untrimmed.withMeta.Treat_Rhizobiales.png

#$lefse_dir/plot_features.py -f one --feature_name "k__Bacteria.p__Proteobacteria" tmp/untrimmed.withMeta.Treat.lefs tmp/untrimmed.withMeta.Treat.lefs.out output_images/untrimmed.withMeta.Treat_Proteobacteria.png

# Plot all biomarkers saving the images in one zip archive ("-f diff" is for plotting biomarkers only, with "-f all" one can plot all input features)
$lefse_dir/plot_features.py -f diff --archive zip tmp/trimmed.withMeta.Crop_Treat_Sample.lefs tmp/trimmed.withMeta.Crop_Treat_Sample.lefs.out output_images/trimmed.withMeta.Crop_Treat_Sample_biomarkers.zip

$lefse_dir/plot_features.py -f diff --archive zip tmp/untrimmed.withMeta.Crop_Treat_Sample.lefs tmp/untrimmed.withMeta.Crop_Treat_Sample.lefs.out output_images/untrimmed.withMeta.Crop_Treat_Sample_biomarkers.zip

### humann: function 
# make diamond db
# diamond makedb --in kegg.reduced.fasta -d kegg.reduced

#perl ~/microbiome_helper/run_pre_humann.pl -p 4 -o pre_humann/ screened/S00298C_r1.screen.fastq.gz
#diamond blastx -q screened/S002987_r1.screen.unpaired.fastq.gz --db ~/humann/data/diamond_db/kegg.reduced  -a tbd.faa -o tbd

## run blastx using diamond
[ -d "humann_profiling_trimmed" ] && rm -fr "humann_profiling_trimmed"

[ -d "pre_humann_trimmed" ] && rm -fr "pre_humann_trimmed"
mkdir humann_profiling_trimmed;
for i in $(echo $(sed "s/\n/ /g" $input_dir/sample.list)); 
do 
  echo $i;
  #time gunzip screened/${i}\_r*.screen.*.fastq.gz --stdout |$microbiome_helper_dir/run_pre_humann.pl -p 12 -o pre_humann/ $1
  time $microbiome_helper_dir/run_pre_humann.pl -p 16 -o pre_humann_trimmed/ screened/${i}\_r*.screen.*paired.fastq.gz
done;

[ -d "pre_humann_untrimmed" ] && rm -fr "pre_humann_untrimmed"
mkdir humann_profiling_untrimmed;
for i in $(echo $(sed "s/\n/ /g" $input_dir/sample.list)); 
do 
  echo $i;
  #time gunzip screened/${i}\_r*.screen.*.fastq.gz --stdout |$microbiome_helper_dir/run_pre_humann.pl -p 12 -o pre_humann/ $1
   time $microbiome_helper_dir/run_pre_humann.pl -p 16 -o pre_humann_untrimmed/ screened/${i}\_r*.screen.fastq.gz
done;

## merge blastx of each sample 
[ -d "pre_humann_trimmed_merged" ] && rm -fr "pre_humann_trimmed_merged"
mkdir pre_humann_trimmed_merged;
for i in $(echo $(sed "s/\n/ /g" $input_dir/sample.list)); 
do 
  echo $i;
  cat pre_humann_trimmed/${i}\_r*.screen.*paired.fastq.txt > pre_humann_trimmed_merged/${i}\_trimmed.fastq.txt
done;

[ -d "pre_humann_untrimmed_merged" ] && rm -fr "pre_humann_untrimmed_merged"
mkdir pre_humann_untrimmed_merged;
for i in $(echo $(sed "s/\n/ /g" $input_dir/sample.list)); 
do 
  echo $i;
  cat pre_humann_untrimmed/${i}\_r*.screen.fastq.txt > pre_humann_untrimmed_merged/${i}\_untrimmed.fastq.txt
done;



#/home/wenchenaafc/microbiome_helper/run_pre_humann.pl -p 4 -o pre_humann/ subset/*
###############################################################################################
# before run scons in humann, remove all files in the /home/wenchenaafc/humann/input/ folder
###############################################################################################
[ "$(ls -A /home/wenchenaafc/humann/input)" ] && echo "/home/wenchenaafc/humann/input Not Empty"|for i in /home/wenchenaafc/humann/input/*; do [ -f $i ] && [ ! -L $i ] && echo "$i exists and is not a symlink"| rm $i; [ -f $i ] && [ -L $i ] && echo "$i exists and is a symlink"| unlink $i; done;

###############################################################################################
# before run scons in humann, MUST put metadata in /home/wenchenaafc/humann/input/ , and name the metadata in hmp_metadata.dat
###############################################################################################
ln -s $input_meta /home/wenchenaafc/humann/input/hmp_metadata.dat


## run scons
cwd=$PWD
ln -s $PWD/pre_humann_trimmed_merged/* /home/wenchenaafc/humann/input/
cd /home/wenchenaafc/humann/
time scons
##interpret humann outputs
#04a...-mpt-... : pathway presence/absence
#04a...-mpm-...: module presence/absence
#04b...-mpt-... : pathway abundance
#04b...-mpm-... : module abundance
mv /home/wenchenaafc/humann/output/*.txt $cwd/humann_profiling_trimmed/
cd $cwd

ln -s $PWD/pre_humann_untrimmed_merged/* /home/wenchenaafc/humann/input/
cd /home/wenchenaafc/humann/
time scons
mv /home/wenchenaafc/humann/output/*.txt $cwd/humann_profiling_untrimmed/
cd $cwd

unlink /home/wenchenaafc/humann/input/hmp_metadata.dat


/home/wenchenaafc/graphlan/graphlan_annotate.py --annot output/04b-*mpm*-graphlan_rings.txt output/04b-*-mpm-*-graphlan_tree.txt mpm.graphlan.xml
/home/wenchenaafc/graphlan/graphlan.py --dpi 200 mpm.graphlan.xml mpm.graphlan.png


/home/wenchenaafc/graphlan/graphlan_annotate.py --annot output/04b-*mpt*-graphlan_rings.txt output/04b-*-mpt-*-graphlan_tree.txt mpt.graphlan.xml
/home/wenchenaafc/graphlan/graphlan.py --dpi 200 mpt.graphlan.xml mpt.graphlan.png





## merge humann outputs
echo $cwd
mkdir humann_output
# trimmed
## annotation files
/home/wenchenaafc/graphlan/graphlan_annotate.py --annot $cwd/humann_profiling_trimmed/04b-hit-keg-mpm-cop-nul-nve-nve-graphlan_rings.txt $cwd/humann_profiling_trimmed/04b-hit-keg-mpm-cop-nul-nve-nve-graphlan_tree.txt humann_output/04b-hit-keg-mpm-cop-nul-nve-nve-graphlan.xml
/home/wenchenaafc/graphlan/graphlan.py --dpi 200 humann_output/04b-hit-keg-mpm-cop-nul-nve-nve-graphlan.xml output_images/04b-hit-keg-mpm-cop-nul-nve-nve-graphlan.png

# untrimmed
/home/wenchenaafc/graphlan/graphlan_annotate.py --annot $cwd/humann_profiling_untrimmed/04b-hit-keg-mpm-cop-nul-nve-nve-graphlan_rings.txt $cwd/humann_profiling_untrimmed/04b-hit-keg-mpm-cop-nul-nve-nve-graphlan_tree.txt humann_output/04b-hit-keg-mpm-cop-nul-nve-nve-graphlan.xml
/home/wenchenaafc/graphlan/graphlan.py --dpi 200 humann_output/04b-hit-keg-mpm-cop-nul-nve-nve-graphlan.xml output_images/04b-hit-keg-mpm-cop-nul-nve-nve-graphlan.png






