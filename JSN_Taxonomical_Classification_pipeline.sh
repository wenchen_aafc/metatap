#/bin/sh
#!/usr/bash

# Start of quality check of raw reads
echo " "
echo "-----------Step1: Quality check of raw reads starts now -------------------------------------------------"
#mkdir /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Raw_FastQC_Report
#mkdir /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Raw_FastQC_Report/Logs
#cd /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Raw_FastQC_Report
#fastqc /isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/JSNsamples/S002994_r1.fastq --outdir=/isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Raw_FastQC_Report > /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Raw_FastQC_Report/Logs/S002994_r1_fastqc.logs 2>&1 
#fastqc /isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/JSNsamples/S002994_r2.fastq --outdir=/isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Raw_FastQC_Report > /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Raw_FastQC_Report/Logs/S002994_r2_fastqc.logs 2>&1
#cd /isilon/biodiversity/users/prabr/hari/
echo "Present working directory is "
echo " "
echo "-----------Step1: Quality check of raw reads successfully completed--------------"
# End of quality check of raw reads

# Start of quality trimming the raw reads from sample using Trimmomatic tool
echo " "
echo "-----------Quality trimming of raw reads using Trimmomatic tool starts now.....(processing)-------------"
#export PATH=/isilon/biodiversity/users/prabr/hari/Trimmomatic-0.33/:$PATH
#mkdir /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Trimmomatic_Output
#mkdir /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Trimmomatic_Output/Logs
#java -jar /isilon/biodiversity/users/prabr/hari/Trimmomatic-0.33/trimmomatic-0.33.jar PE -threads 20 -phred33 /isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/JSNsamples/S002994_r1.fastq /isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/JSNsamples/S002994_r2.fastq /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Trimmomatic_Output/output_forward_paired.fq.gz /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Trimmomatic_Output/output_forward_unpaired.fq.gz /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Trimmomatic_Output/output_reverse_paired.fq.gz /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Trimmomatic_Output/output_reverse_unpaired.fq.gz LEADING:25 TRAILING:25 SLIDINGWINDOW:4:15 MINLEN:36 > /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Trimmomatic_Output/Logs/Trimmomatic.logs 2>&1
echo " "
echo "-----------Quality trimming of raw reads using Trimmomatic tool successfully completed------------------"

# End of quality trimming the raw reads using Trimmomatic tool


# Start of quality check of trimmed reads from Trimmomatic analysis
echo " "
echo "-----------Quality check of trimmed reads from Trimmomatic analysis starts now....(processing)----------"
#mkdir /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Trimmed_FastQC_Report
#mkdir /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Trimmed_FastQC_Report/Logs
#cd /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Trimmed_FastQC_Report
#fastqc /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Trimmomatic_Output/output_forward_paired.fq.gz --outdir=/isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Trimmed_FastQC_Report > /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Trimmed_FastQC_Report/Logs/output_forward_paired.logs 2>&1
#fastqc /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Trimmomatic_Output/output_reverse_paired.fq.gz --outdir=/isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Trimmed_FastQC_Report > /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Trimmed_FastQC_Report/Logs/output_reverse_paired.logs 2>&1
#cd /isilon/biodiversity/users/prabr/hari/
echo " "
echo "-----------Quality check of trimmed reads from Trimmomatic analysis successfully completed--------------"
# End of quality check of trimmed reads from Trimmomatic analysis

# Start of decontamination step based on trimmed reads
echo " "
echo "-----------Decontamination of trimmed reads starts now......(processing)--------------------------------"
#export PATH=/isilon/biodiversity/users/prabr/hari/fastq_screen_v0.4.4/:$PATH
#mkdir /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Fastq_Screen_Output
#mkdir /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Fastq_Screen_Output/Logs
#perl /isilon/biodiversity/users/prabr/hari/fastq_screen_v0.4.4/fastq_screen --threads 8 --nohits --aligner bowtie2 --bowtie2 "--local" --conf /isilon/biodiversity/users/prabr/hari/fastq_screen_v0.4.4/fastq_screen.conf --paired /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Trimmomatic_Output/output_forward_paired.fq.gz /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Trimmomatic_Output/output_reverse_paired.fq.gz --outdir /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Fastq_Screen_Output > /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Fastq_Screen_Output/Logs/Fastq_Screen.logs 2>&1
echo ""
echo "------------Decontamination of trimmed reads successfully completed ------------------------------------"
# End of decontamination step based on trimmed reads


# Start of Assembling the trimmed reads using metAMOS pipeline
echo " "
echo "------------Assembling of trimmed reads process starts now.....(processing)-----------------------------"
gunzip /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Fastq_Screen_Output/output_forward_paired_no_hits_file.1.fastq.gz
gunzip /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Fastq_Screen_Output/output_reverse_paired_no_hits_file.2.fastq.gz
mkdir /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/metAMOS_Output/
mkdir /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/metAMOS_Output/Logs
cd /isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/metAMOS_Output/
export PATH=/isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/:$PATH
/isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/initPipeline -q -1 /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Fastq_Screen_Output/output_forward_paired_no_hits_file.1.fastq  /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Fastq_Screen_Output/output_reverse_paired_no_hits_file.2.fastq -d /isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/metAMOS_Output/P_2015* -i 30-150
/isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/runPipeline -q -a soap -p 15 -d /isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/metAMOS_Output/P_2015* -k 55 -f Preprocess,Assemble,MapReads,Propagate,Postprocess -n Annotate,FindORFs,FunctionalAnnotation,Classify,Abundance,FindScaffoldORFS,Scaffold > /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/metAMOS_Output/Logs/metAMOS.logs 2>&1
cd /isilon/biodiversity/users/prabr/hari/
cp /isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/metAMOS_Output/P_2015*/Assemble/out/proba.asm.contig /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/metAMOS_Output/proba.asm.contig.fa
cp /isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/metAMOS_Output/P_2015*/Assemble/out/proba.contig.cnt /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/metAMOS_Output/proba.contig.cnt
echo " "
echo "------------Assembling of trimmed reads process successfully completed----------------------------------"
# End of Assembling the trimmed reads using metAMOS pipeline


# Start of annotation step of reads

mkdir /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Kraken_Output/
mkdir /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Kraken_Output/Logs
echo " "
echo "------------Annotation of contigs based on kraken database starts now....(processing)---------------------"
echo " "
export PATH=/isilon/biodiversity/users/prabr/hari/kraken/:$PATH
cd /isilon/biodiversity/users/prabr/hari/kraken/
kraken --db /isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/Utilities/DB/kraken --quick --preload --min-hits 5 --threads 20 --output /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Kraken_Output/Kraken_Contig_Output.hits /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/metAMOS_Output/proba.asm.contig.fa > /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Kraken_Output/Logs/Kraken.logs 2>&1
cd /isilon/biodiversity/users/prabr/hari/
echo " "
echo "------------Annotation of contigs based on kraken database successfully completed------------------------"
# End of annotation step of reads

# Start of translating the kraken output to taxonomical classification
echo " "
echo "------------Taxonomical classification process starts now.....(processing)-------------------------------"
export PATH=/isilon/biodiversity/users/prabr/hari/kraken/scripts/kraken-translate:$PATH
#/isilon/biodiversity/users/prabr/hari/kraken/scripts/kraken-translate --db /isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/Utilities/DB/kraken/ /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Kraken_Output/Kraken_Contig_Output.hits > /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Kraken_Output/Kraken_Taxonomical_Output.labels
/isilon/biodiversity/users/prabr/hari/kraken/scripts/kraken-translate --mpa-format --db /isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/Utilities/DB/kraken/ /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Kraken_Output/Kraken_Contig_Output.hits > /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Kraken_Output/Kraken_Taxonomical_Output_With_Rank.tab
echo " "
echo "------------Taxonomical classification process successfully completed------------------------------------"
# End of translating the kraken output to taxonomical classification

# Start of editing the kraken output with taxonomical rank
echo " "
echo "------------Editing of kraken output for taxonomical classification process starts now...(processing)----"
perl /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Kraken_Taxonomical_Output_Editor.pl /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Kraken_Output/Kraken_Taxonomical_Output_With_Rank.tab /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Kraken_Output/
echo " "
echo "------------Editing of kraken output for Taxonomical classification process successfully completed-------"
# End of editing the kraken output with taxonomical rank


# Start of generating the taxonomical OTU table
echo " "
echo "------------Taxonomical OTU table generation process starts now.....(processing)-----------------------------"
perl /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Taxonomical_OTU_Generator.pl /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Kraken_Output/Taxonomical_Output_With_Rank_Edited.tab /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/metAMOS_Output/proba.contig.cnt /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Kraken_Output/
echo " "
echo "------------Taxonomical OTU table generation successfully completed------------------------------------------"
# End of generating the taxonomical OTU table


# Start of generating krona files for taxonomical classification
echo " "
echo "------------Generating krona files for taxonomical classification starts now...(processing)------------"
mkdir /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Krona_Output/
mkdir /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Krona_Output/Logs
export PATH=/isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/KronaTools/bin:$PATH 
cd /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Krona_Output/
cp /isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/metAMOS_Output/P_2015*/Assemble/out/proba.contig.cnt /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/metAMOS_Output/proba.contig.cnt
perl /isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/KronaTools/ImportKraken.pl -c -i -f /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/metAMOS_Output/proba.asm.contig.fa /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Kraken_Output/Kraken_Contig_Output.hits:/isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/metAMOS_Output/proba.contig.cnt:class > /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Krona_Output/Logs/krona.logs 2>&1
cd /isilon/biodiversity/users/prabr/hari/
echo " "
echo "------------Generation of krona files for taxonomical classification successfully completed------------"
# End of generating krona files for taxonomical classification
