#! /bin/bash

[ -f "log" ] && rm -fr "log";
exec >  >(tee -ai log)
exec 2> >(tee -ai log >&2)

set -x
#all tools: https://bitbucket.org/biobakery/biobakery/wiki/Home
#wget https://cran.r-project.org/src/contrib/infotheo_1.2.0.tar.gz
# sudo R CMD INSTALL --build infotheo_1.2.0.tar.gz 
#  sudo R CMD INSTALL --build ccrepe_1.1.1.tar.gz 
# for installing dependencies, check file shotgun_pipeline.sh

################################################
# index wheat genome (or other genomes for screening purposes
################################################
# cd ~/Desktop/Data/wheat_genome
# time bash ~/bowtie2-2.2.6/make_wheatgenome1to7ABD_index_use_fasta.sh

################################################
# check commands location
################################################
metaphlan_dir='/home/wenchenaafc/metaphlan'; 
[ ! -d "$metaphlan_dir" ] && echo "metaphlan direcotry doesn't exist" && exit;
metaphlan_script=$metaphlan_dir/metaphlan.py; 
[ ! -f "$metaphlan_script" ] && echo "path to metaphlan.py doesn't exist" && exit;
metaphlan_db="$metaphlan_dir/bowtie2db/mpa";
metaphlan_merge="$metaphlan_dir/utils/merge_metaphlan_tables.py";
[ ! -f "$metaphlan_merge" ] && echo "path to merge_metaphlan_tables.py doesn't exist" && exit;
lefse_dir='/home/wenchenaafc/lefse';
[ ! -d "$lefse_dir" ] && echo "lefse directory doesn't exist" && exit;
microbiome_helper_dir='/home/wenchenaafc/microbiome_helper';
[ ! -d "$microbiome_helper_dir" ] && echo "microbiome directory doesn't exist" && exit;
graphlan_dir='/home/wenchenaafc/graphlan';
[ ! -d "$graphlan_dir" ] && echo "graphlan direcotry doesn't exist" && exit;
humann_dir='/home/wenchenaafc/humann';
[ ! -d "$humann_dir" ] && echo "humann direcotry doesn't exist" && exit;
metaphlan2_dir='/home/wenchenaafc/metaphlan2'; 
[ ! -d "$metaphlan2_dir" ] && echo "metaphlan2 direcotry doesn't exist" && exit;

################################################
# setup input output and metadata locations
################################################
# tar xvf 140520_7001410_0133_BC4CPDACXX_lane4.tar | mv samples lane4;
# tar xvf 140520_7001410_0133_BC4CPDACXX_lane5.tar | mv samples lane5;

input_dir="/media/wenchenaafc/Data/140520_7001410_0133_BC4CPDACXX/input";
input_meta="/home/wenchenaafc/Data/140520_7001410_0133_BC4CPDACXX/meta.tab";
output_dir="/media/wenchenaafc/Data/140520_7001410_0133_BC4CPDACXX/output_trim_then_screen"

################################################
# check if input dir and metadata exists and empty,
################################################
echo $input_dir; [ ! -d $input_dir ] &&  echo "input directory $input_dir doesn't exist" && exit 1; [ -d $input_dir ] && [ "${input_dir:0:1}" != / ] && echo "input directory $input_dir should be absolute path" && input_dir=$(cd "$(dirname "$input_dir")"; pwd)/$(basename "$input_dir") || input_dir=$input_dir; echo $input_dir;

echo $input_meta; [ ! -f $input_meta ] &&  echo "metadata $input_meta doesn't exist" && exit 1; [ -f $input_meta ] && [ "${input_meta:0:1}" != / ] && echo "path to metadata file $input_meta should be absolute path" && input_meta=$(cd "$(dirname "$input_meta")"; pwd)/$(basename "$input_meta") || input_meta=$input_meta; echo $input_meta;

################################################
# check if output dir exists and empty,
################################################
#if [ -d $output_dir ]; then
    # the directory exists
#    [ "$(ls -A $output_dir)" ] && echo "output directory $output_dir is not Empty" && echo "will save output to $output_dir\_0" && mkdir $output_dir\_0 && output_dir=$output_dir\_0 && output_dir=$(cd "$(dirname "$output_dir")"; pwd)/$(basename "$output_dir") || echo "output directory $output_dir already exists but is empty"; output_dir=$output_dir && output_dir=$(cd "$(dirname "$output_dir")"; pwd)/$(basename "$output_dir"); 
#  else 
#    echo "output directory $output_dir doesn't exist" && mkdir $output_dir && output_dir=$(cd "$(dirname "$output_dir")"; pwd)/$(basename "$output_dir");
#fi

echo "input_dir is: $input_dir";
echo "metadata is: $input_meta";
echo "output_dir is: $output_dir";


################################################
# get sample list
################################################
[ -f "$input_dir/sample.list" ] && rm $input_dir/sample.list;
ls $input_dir/*fastq.gz|while read line; do echo $line; full=$(basename $line); base=${full%_[rR][1-2]*}; echo $full; echo $base >> temp; done; sort temp|uniq >> $input_dir/sample.list; rm temp; 


################################################
# Go to output folder
################################################
cd $output_dir;

################################################
## trim raw reads using Trimmomatic
################################################
#mkdir trimmed;
#for i in $(echo $(sed "s/\n/ /g" $input_dir/sample.list)); 
#do 
#  echo $i;
#  time java -jar ~/Trimmomatic-0.35/trimmomatic-0.35.jar PE -threads 20 -trimlog trimmed/$i.trimLog -phred33 $input_dir/$i\_[rR]1.fastq.gz $input_dir/$i\_[rR]2.fastq.gz trimmed/$i\_r1.paired.fastq.gz trimmed/$i\_r1.unpaired.fastq.gz trimmed/$i\_r2.paired.fastq.gz trimmed/$i\_r2.unpaired.fastq.gz LEADING:25 TRAILING:25 SLIDINGWINDOW:1:25 MINLEN:50 AVGQUAL:25;
  # real	170m4.330s (1.7G*2 S002984_r[1-2].fastq.gz)
  # user	31m58.951s
  # sys	34m18.322s

#done;


################################################
## screen unmatched sequence using bowtie
### run bowtie2 to screen out wheat seqs
################################################
mkdir screened_unmatched
for i in $(echo $(sed "s/\n/ /g" $input_dir/sample.list)); 
do 
  echo $i;
  #### write pairs that didn't align concordantly to genome
  time bowtie2 -x /home/wenchenaafc/Data/wheat_genome/wg1to7ABD -p 20 -1 trimmed/$i\_r1.paired.fastq.gz -2 trimmed/$i\_r2.paired.fastq.gz --un-conc-gz screened_unmatched/$i.fastq.gz >> /dev/null; mv screened_unmatched/$i.fastq.1.gz screened_unmatched/$i\_r1.paired.screen.fastq.gz; mv screened_unmatched/$i.fastq.2.gz screened_unmatched/$i\_r2.paired.screen.fastq.gz;
  #### write unpaired reads that didn't align to genome
  time bowtie2 -x /home/wenchenaafc/Data/wheat_genome/wg1to7ABD -p 20 --un-gz screened_unmatched/$i\_r1.unpaired.screen.fastq.gz -U <(zcat trimmed/$i\_r1.unpaired.fastq.gz) >> /dev/null;
  time bowtie2 -x /home/wenchenaafc/Data/wheat_genome/wg1to7ABD -p 20 --un-gz screened_unmatched/$i\_r2.unpaired.screen.fastq.gz -U <(zcat trimmed/$i\_r2.unpaired.fastq.gz) >> /dev/null;
done;

################################################
## screen matched sequence using bowtie
### run bowtie2 to screen sequences matched wheat seqs
################################################
mkdir screened_matched
for i in $(echo $(sed "s/\n/ /g" $input_dir/sample.list)); 
do 
  echo $i;
  #### write pairs that align concordantly to genome
  time bowtie2 -x /home/wenchenaafc/Data/wheat_genome/wg1to7ABD -p 20 -1 trimmed/$i\_r1.paired.fastq.gz -2 trimmed/$i\_r2.paired.fastq.gz --al-conc-gz screened_matched/$i.fastq.gz >> /dev/null; mv screened_matched/$i.fastq.1.gz screened_matched/$i\_r1.paired.screen.fastq.gz; mv screened_matched/$i.fastq.2.gz screened_matched/$i\_r2.paired.screen.fastq.gz;
  #### write unpaired reads that align to genome
  time bowtie2 -x /home/wenchenaafc/Data/wheat_genome/wg1to7ABD -p 20 --al-gz screened_matched/$i\_r1.unpaired.screen.fastq.gz -U <(zcat trimmed/$i\_r1.unpaired.fastq.gz) >> /dev/null;
  time bowtie2 -x /home/wenchenaafc/Data/wheat_genome/wg1to7ABD -p 20 --al-gz screened_matched/$i\_r2.unpaired.screen.fastq.gz -U <(zcat trimmed/$i\_r2.unpaired.fastq.gz) >> /dev/null;
done;


#sample="S00
#pair="r1 r2"; for i in $pair; do echo S002986_${i}.screen.fastq.bz2; bzip2 -dk S002986_${i}.screen.fastq.bz2; gzip -1kf S002986\_${i}.screen.fastq; done
#tar -cvjSf test.tar.bz2 *.screen.fastq.gz # 3.3G no advantage in saving space
#tar -pczf test.tar.gz *.screen.fastq.gz # 3.3G no advantage in saving space

################################################
### if needed, further filter the fastq by length
################################################
if [ -d "screened_unmatched_fillen" ];
then 
  echo "screened_unmatched_fillen directory already exists";
else 
  mkdir screened_unmatched_fillen && echo "make screened_unmatched_fillen directory";
fi;

for i in $(echo $(sed "s/\n/ /g" $input_dir/sample.list)); 
do 
  echo $i;
  awk 'BEGIN {OFS = "\n"} {header = $0 ; getline seq ; getline qheader ; getline qseq ; if (length(seq) >= 75 && length(seq) <= 101) {print header, seq, qheader, qseq}}' <(zcat screened_unmatched/${i}\_[rR][1-2]*.*paired.screen.fastq.gz) > ${i}\_filtered_75.fastq;
  gzip ${i}\_filtered_75.fastq
  mv ${i}\_filtered_75.fastq.gz screened_unmatched_fillen/;
done;


if [ -d "screened_matched_fillen" ];
then 
  echo "screened_matched_fillen directory already exists";
else 
  mkdir screened_matched_fillen && echo "make screened_matched_fillen directory";
fi;

for i in $(echo $(sed "s/\n/ /g" $input_dir/sample.list)); 
do 
  echo $i;
  awk 'BEGIN {OFS = "\n"} {header = $0 ; getline seq ; getline qheader ; getline qseq ; if (length(seq) >= 75 && length(seq) <= 101) {print header, seq, qheader, qseq}}' <(zcat screened_matched/${i}\_[rR][1-2]*.*paired.screen.fastq.gz) > ${i}\_filtered_75.fastq;
  gzip ${i}\_filtered_75.fastq
  mv ${i}\_filtered_75.fastq.gz screened_matched_fillen/;
done;


################################################
## metaphlan
################################################
### metaphlan
if [ -d "bowtie_out" ];
then 
  echo "bowtie_out directory already exists";
else 
  mkdir bowtie_out && echo "make bowtie_out directory";
fi;

if [ -d "profiled_samples" ];
then 
  echo "profiled_samples directory already exists"
else 
  mkdir profiled_samples && echo "make profiled_samples directory";
fi;

for i in $(echo $(sed "s/\n/ /g" $input_dir/sample.list)); 
do 
  echo $i;
  # non-local presets did not classify anything
  gunzip screened_unmatched/${i}\_[rR][1-2]*.*paired.screen.fastq.gz --stdout | $metaphlan_script --bowtie2db $metaphlan_dir/bowtie2db/mpa --bt2_ps sensitive --input_type multifastq --bowtie2out bowtie_out/${i}\_s.bt2out > profiled_samples/${i}\_s.txt
  gunzip screened_unmatched/${i}\_[rR][1-2]*.*paired.screen.fastq.gz --stdout | $metaphlan_script --bowtie2db $metaphlan_dir/bowtie2db/mpa --bt2_ps very-sensitive --input_type multifastq --bowtie2out bowtie_out/${i}\_vs.bt2out > profiled_samples/${i}\_vs.txt
  gunzip screened_unmatched/${i}\_[rR][1-2]*.*paired.screen.fastq.gz --stdout | $metaphlan_script --bowtie2db $metaphlan_dir/bowtie2db/mpa --bt2_ps sensitive-local --input_type multifastq --bowtie2out bowtie_out/${i}\_sl.bt2out > profiled_samples/${i}\_sl.txt
  gunzip screened_unmatched/${i}\_[rR][1-2]*.*paired.screen.fastq.gz --stdout | $metaphlan_script --bowtie2db $metaphlan_dir/bowtie2db/mpa --bt2_ps very-sensitive-local --input_type multifastq --bowtie2out bowtie_out/${i}\_vsl.bt2out > profiled_samples/${i}\_vsl.txt
done

## metaphlan2
if [ -d "bowtie_out_metaphlan2" ];
then 
  echo "bowtie_out_metaphlan2 directory already exists";
else 
  mkdir bowtie_out_metaphlan2 && echo "make bowtie_out_metaphlan2 directory";
fi;

if [ -d "profiled_samples_metaphlan2" ];
then 
  echo "profiled_samples_metaphlan2 directory already exists"
else 
  mkdir profiled_samples_metaphlan2 && echo "make profiled_samples_metaphlan2 directory";
fi;

for i in $(echo $(sed "s/\n/ /g" $input_dir/sample.list)); 
do 
  echo $i;
  # non-local presets did not classify anything
  gunzip screened_unmatched/${i}\_[rR][1-2]*.*paired.screen.fastq.gz --stdout | python $metaphlan2_dir/metaphlan2.py --mpa_pkl $metaphlan2_dir/db_v20/mpa_v20_m200.pkl --bowtie2db $metaphlan2_dir/db_v20/mpa_v20_m200 --bt2_ps sensitive --input_type multifastq -t rel_ab --bowtie2out bowtie_out_metaphlan2/${i}\_s.bt2out > profiled_samples_metaphlan2/${i}\_s.txt
  gunzip screened_unmatched/${i}\_[rR][1-2]*.*paired.screen.fastq.gz --stdout | python $metaphlan2_dir/metaphlan2.py --mpa_pkl $metaphlan2_dir/db_v20/mpa_v20_m200.pkl --bowtie2db $metaphlan2_dir/db_v20/mpa_v20_m200 --bt2_ps very-sensitive --input_type multifastq -t rel_ab --bowtie2out bowtie_out_metaphlan2/${i}\_vs.bt2out > profiled_samples_metaphlan2/${i}\_vs.txt
  gunzip screened_unmatched/${i}\_[rR][1-2]*.*paired.screen.fastq.gz --stdout | python $metaphlan2_dir/metaphlan2.py --mpa_pkl $metaphlan2_dir/db_v20/mpa_v20_m200.pkl --bowtie2db $metaphlan2_dir/db_v20/mpa_v20_m200 --bt2_ps sensitive-local --input_type multifastq -t rel_ab --bowtie2out bowtie_out_metaphlan2/${i}\_sl.bt2out > profiled_samples_metaphlan2/${i}\_sl.txt
  gunzip screened_unmatched/${i}\_[rR][1-2]*.*paired.screen.fastq.gz --stdout | python $metaphlan2_dir/metaphlan2.py --mpa_pkl $metaphlan2_dir/db_v20/mpa_v20_m200.pkl --bowtie2db $metaphlan2_dir/db_v20/mpa_v20_m200 --bt2_ps very-sensitive-local --input_type multifastq -t rel_ab --bowtie2out bowtie_out_metaphlan2/${i}\_vsl.bt2out > profiled_samples_metaphlan2/${i}\_vsl.txt
done


#gunzip screened_unmatched/S002984_[rR][1-2]*.*paired.screen.fastq.gz --stdout | python $metaphlan2_dir/metaphlan2.py --mpa_pkl $metaphlan2_dir/db_v20/mpa_v20_m200.pkl --bowtie2db $metaphlan2_dir/db_v20/mpa_v20_m200 --bt2_ps very-sensitive --input_type multifastq -t rel_ab --bowtie2out S002984.bowtie2_out_vs.bz2 -o S002984_vs.txt
#gunzip screened_unmatched/S002984_[rR][1-2]*.*paired.screen.fastq.gz --stdout | python $metaphlan2_dir/metaphlan2.py --mpa_pkl $metaphlan2_dir/db_v20/mpa_v20_m200.pkl --bowtie2db $metaphlan2_dir/db_v20/mpa_v20_m200 --bt2_ps very-sensitive-local --input_type multifastq -t rel_ab --bowtie2out S002984.bowtie2_out_vsl.bz2 -o S002984_vsl.txt
#gunzip screened_unmatched/S002984_[rR][1-2]*.*paired.screen.fastq.gz --stdout | python $metaphlan2_dir/metaphlan2.py --mpa_pkl $metaphlan2_dir/db_v20/mpa_v20_m200.pkl --bowtie2db $metaphlan2_dir/db_v20/mpa_v20_m200 --bt2_ps sensitive --input_type multifastq -t rel_ab --bowtie2out S002984.bowtie2_out_s.bz2 -o S002984_s.txt
#gunzip screened_unmatched/S002984_[rR][1-2]*.*paired.screen.fastq.gz --stdout | python $metaphlan2_dir/metaphlan2.py --mpa_pkl $metaphlan2_dir/db_v20/mpa_v20_m200.pkl --bowtie2db $metaphlan2_dir/db_v20/mpa_v20_m200 --bt2_ps sensitive-local --input_type multifastq -t rel_ab --bowtie2out S002984.bowtie2_out_sl.bz2 -o S002984_sl.txt

# kraken
if [ -d "kraken_mini_output" ];
then 
  echo "kraken_mini_output directory already exists";
else 
  mkdir kraken_mini_output && echo "make kraken_mini_output directory";
fi;

kraken --fastq-input --db ~/Data/metAMOS-1.5rc3/minikraken_20141208 --quick --preload --min-hits 5 --threads 4 --output kraken_mini_output/S002984_kraken.hits <(zcat screened_unmatched/S002984_[rR][1-2]*.*paired.screen.fastq.gz ) 

kraken-report --db ~/Data/metAMOS-1.5rc3/minikraken_20141208 --show-zeros < kraken_mini_output/S002984_kraken.hits > kraken_mini_output/S002984_kraken.report

awk -F"\t" '{if($2>0) print $0}' kraken_mini_output/S002984_kraken.report > kraken_mini_output/S002984_kraken.report.fil

cut -f2,3  kraken_mini_output/S002984_kraken.hits > kraken_mini_output/S002984_kraken.hits.in

perl ~/KronaTools-2.6/scripts/ImportTaxonomy.pl kraken_mini_output/S002984_kraken.hits.in  -o kraken_mini_output/S002984_krona.html

# kraken --db ~/Data/metAMOS-1.5rc3/minikraken_20141208 --quick --preload --min-hits 5 --threads 4 --output kraken_mini_output/S002984_kraken.hits --fastq-input <(seqtk seq -A <(zcat screened_unmatched/S002984_[rR][1-2]*.*paired.screen.fastq.gz ) ) > tmp/S002984_kraken.logs 

kraken-translate --mpa-format --db ~/Data/metAMOS-1.5rc3/minikraken_20141208 kraken_mini_output/S002984_kraken.hits > kraken_mini_output/S002984_kraken_taxonomy.tab

#kraken-translate --db ~/Data/metAMOS-1.5rc3/minikraken_20141208 kraken_mini_output/S002984_kraken.hits > kraken_mini_output/S002984_kraken_taxonomy_nonmpa.tab

python ~/ktoolu/kt_summarize.py --include-unclassified --draw-krona-plot kraken_mini_output/S002984_kraken.hits  

ktImportTaxonomy krakenoutput.txt -o krona4.html


################################################
## humann: function /pathway analyses
################################################
# make diamond db
# diamond makedb --in kegg.reduced.fasta -d kegg.reduced
# blastdb
#perl ~/microbiome_helper/run_pre_humann.pl -p 4 -o pre_humann/ screened/S00298C_r1.screen.fastq.gz
#diamond blastx -q screened/S002987_r1.screen.unpaired.fastq.gz --db ~/humann/data/diamond_db/kegg.reduced  -a tbd.faa -o tbd

## run blastx using diamond
[ -d "pre_humann_unmatch" ] && rm -fr "pre_humann_unmatch";
for i in $(echo $(sed "s/\n/ /g" $input_dir/sample.list)); 
do 
  echo $i;
  #time gunzip screened/${i}\_r*.screen.*.fastq.gz --stdout |$microbiome_helper_dir/run_pre_humann.pl -p 12 -o pre_humann/ $1
  time $microbiome_helper_dir/run_pre_humann.pl -p 20 -o pre_humann_unmatch/ screened_unmatched/${i}\_[rR][1-2]*.*paired.screen.fastq.gz
done;

[ -d "pre_humann_match" ] && rm -fr "pre_humann_match"
for i in $(echo $(sed "s/\n/ /g" $input_dir/sample.list)); 
do 
  echo $i;  
  time $microbiome_helper_dir/run_pre_humann.pl -p 20 -o pre_humann_match/ screened_matched/${i}\_[rR][1-2]*.*paired.screen.fastq.gz
done;

## merge blastx of each sample 
[ -d "pre_humann_match_merged" ] && rm -fr "pre_humann_match_merged"
mkdir pre_humann_match_merged;
for i in $(echo $(sed "s/\n/ /g" $input_dir/sample.list)); 
do 
  echo $i;
  cat pre_humann_match/${i}\_r*.screen.fastq.txt > pre_humann_match_merged/${i}\_matched.fastq.txt
done;

[ -d "pre_humann_unmatch_merged" ] && rm -fr "pre_humann_unmatch_merged"
mkdir pre_humann_unmatch_merged;
for i in $(echo $(sed "s/\n/ /g" $input_dir/sample.list)); 
do 
  echo $i;
  cat pre_humann_unmatch/${i}\_r*.screen.fastq.txt > pre_humann_unmatch_merged/${i}\_unmatched.fastq.txt
done;


###############################################################################################
## run scons
###############################################################################################
# make new directories
[ -d "humann_profiling_unmatch" ] && rm -fr "humann_profiling_unmatch"
[ -d "humann_profiling_match" ] && rm -fr "humann_profiling_match"
mkdir humann_profiling_unmatch;
mkdir humann_profiling_match;


###############################################################################################
# before run scons in humann, remove all files in the $humann_dir/input/ folder
###############################################################################################
if [ "$(ls -A $humann_dir/input)" ] 
then 
  echo "$humann_dir/input Not Empty";
  for i in $humann_dir/input/*; 
  do 
    if [ -f $i ] && [ ! -L $i ] 
    then 
      echo "$i exists and is not a symlink, remove $i" && rm $i; 
    else 
      echo "$i exists and is a symlink, unlink $i" && unlink $i; 
    fi;
  done;
fi; 

if [ "$(ls -A $humann_dir/output)" ] 
then 
  echo "$humann_dir/output Not Empty";
  for i in $humann_dir/output/*; 
  do 
    if [ -f $i ] && [ ! -L $i ] 
    then 
      echo "$i exists and is not a symlink, remove $i" && rm $i; 
    else 
      echo "$i exists and is a symlink, unlink $i" && unlink $i; 
    fi;
  done;
fi; 

###############################################################################################
# before run scons in humann, MUST put metadata in $humann_dir/input/ & $humann_dir/output/, and name the metadata in hmp_metadata.dat
###############################################################################################
if [ -f "$humann_dir/input/hmp_metadata.dat" ]
then 
  rm $humann_dir/input/hmp_metadata.dat
fi;

if [ -L "$humann_dir/input/hmp_metadata.dat" ] 
then
  unlink $humann_dir/input/hmp_metadata.dat
fi

cwd=$PWD
ln -s $PWD/pre_humann_unmatch_merged/*_unmatched.fastq.txt $humann_dir/input/
cd $humann_dir/
time scons
mv $humann_dir//output/*.txt $cwd/humann_profiling_unmatch/
unlink $humann_dir/input/hmp_metadata.dat
cd $cwd

###############################################################################################
# before run scons in humann, remove all files in the $humann_dir/input/ & $humann_dir/output/ folder
###############################################################################################
if [ "$(ls -A $humann_dir/input)" ] 
then 
  echo "$humann_dir/input Not Empty";
  for i in $humann_dir/input/*; 
  do 
    if [ -f $i ] && [ ! -L $i ] 
    then 
      echo "$i exists and is not a symlink, remove $i" && rm $i; 
    else 
      echo "$i exists and is a symlink, unlink $i" && unlink $i; 
    fi;
  done;
fi; 

if [ "$(ls -A $humann_dir/output)" ] 
then 
  echo "$humann_dir/output Not Empty";
  for i in $humann_dir/output/*; 
  do 
    if [ -f $i ] && [ ! -L $i ] 
    then 
      echo "$i exists and is not a symlink, remove $i" && rm $i; 
    else 
      echo "$i exists and is a symlink, unlink $i" && unlink $i; 
    fi;
  done;
fi; 

###############################################################################################
# before run scons in humann, MUST put metadata in $humann_dir/input/ , and name the metadata in hmp_metadata.dat
###############################################################################################
if [ -f "$humann_dir/input/hmp_metadata.dat" ]
then 
  rm $humann_dir/input/hmp_metadata.dat
fi;

if [ -L "$humann_dir/input/hmp_metadata.dat" ] 
then
  unlink $humann_dir/input/hmp_metadata.dat
fi

ln -s $input_meta $humann_dir/input/hmp_metadata.dat


ln -s $PWD/pre_humann_match_merged/*_matched.fastq.txt $humann_dir/input/

cd $humann_dir/
time scons
mv $humann_dir/output/*.txt $cwd/humann_profiling_match/

unlink $humann_dir/input/hmp_metadata.dat
cd $cwd


## output of humann
##interpret humann outputs
#04a...-mpt-... : pathway presence/absence
#04a...-mpm-...: module presence/absence
#04b...-mpt-... : pathway abundance
#04b...-mpm-... : module abundance

###############################################################################################
## analyses and visualization
###############################################################################################

if [ -d "tmp" ];
then 
  echo "tmp directory already exists";
else 
  mkdir tmp && echo "make tmp directory";
fi;


if [ -d "output_images" ];
then 
  echo "output_images directory already exists";
else 
  mkdir output_images && echo "make output_images directory";
fi;

if [ -d "metaphlan_output" ];
then 
  echo "metaphlan_output directory already exists";
else 
  mkdir metaphlan_output && echo "make metaphlan_output directory";
fi;

#The resulting table contains relative abundances with microbial clades as rows and samples as columns.
$metaphlan_dir/utils/merge_metaphlan_tables.py $(ls profiled_samples/*l.txt|sort) > metaphlan_output/microbiom_merged_abundance.txt

$metaphlan_dir/plotting_scripts/metaphlan_hclust_heatmap.py -c bbcry --top 25 --minv 0.1 -s log -m average -d braycurtis -f correlation --in metaphlan_output/microbiom_merged_abundance.txt --out output_images/microbiom_merged.heatmap.pdf

$metaphlan_dir/plotting_scripts/metaphlan2graphlan.py metaphlan_output/microbiom_merged_abundance.txt --tree_file tmp/microbiom_merged.tree.txt --annot_file tmp/microbiom_merged.annot.txt
graphlan_annotate.py --annot tmp/microbiom_merged.annot.txt tmp/microbiom_merged.tree.txt tmp/microbiom_merged.xml
graphlan.py --format pdf tmp/microbiom_merged.xml output_images/microbiom_merged.pdf

python $metaphlan2_dir/utils/export2graphlan/export2graphlan.py --skip_rows 1,2 -i metaphlan_output/microbiom_merged_abundance.txt --tree tmp/microbiom_merged_abundance.tree.txt --annotation tmp/microbiom_merged_abundance.annot.txt


## lefse
perl ~/Bitbucket/metatap/merge_metadata_sel_with_metaphlan_merged_profile.pl metaphlan_output/microbiom_merged_abundance.txt $input_meta Crop,Treat,Sample Sample

# first LEfSe step: format the input specifying that the class info is in the first row
# Crop, Treat, Sample
$lefse_dir/format_input.py metaphlan_output/microbiom_merged_abundance.withMeta.Crop_Treat_Sample.tab tmp/microbiom_merged_abundance.withMeta.Crop_Treat_Sample.lefs -c 3 -s 2 -u 1 -o 1000000

# run the LEfSe biomarker discovery tool with default options apart for the 
# threshold on the LDA effect size which is increaset to 4

$lefse_dir/run_lefse.py tmp/microbiom_merged_abundance.withMeta.Crop_Treat_Sample.lefs tmp/microbiom_merged_abundance.withMeta.Crop_Treat_Sample.lefs.out -l 2 -y 1 -a 0.01 -w 0.01

# Plot the resulting list of biomarkers with the corresponsing effect size
# Crop, Treat, Sample
$lefse_dir/plot_res.py --format pdf tmp/microbiom_merged_abundance.withMeta.Crop_Treat_Sample.lefs.out output_images/microbiom_merged_abundance.withMeta.Crop_Treat_Sample.lefs_biomarkers.pdf

# Plot the biomarkers on the underlying cladogram
# Crop, Treat, Sample
$lefse_dir/plot_cladogram.py --format pdf tmp/microbiom_merged_abundance.withMeta.Crop_Treat_Sample.lefs.out output_images/microbiom_merged_abundance.withMeta.Crop_Treat_Sample.lefs_cladogram.pdf 

# Plot all biomarkers saving the images in one zip archive ("-f diff" is for plotting biomarkers only, with "-f all" one can plot all input features)
$lefse_dir/plot_features.py --format pdf -f diff --archive zip tmp/microbiom_merged_abundance.withMeta.Crop_Treat_Sample.lefs tmp/microbiom_merged_abundance.withMeta.Crop_Treat_Sample.lefs.out output_images/microbiom_merged_abundance.withMeta.Crop_Treat_Sample_each_biomarker.zip

#########################################
## humann outputs
#########################################
## microbiom
$graphlan_dir/graphlan_annotate.py --annot humann_profiling_unmatch/04b-*mpm*-graphlan_rings.txt humann_profiling_unmatch/04b-*-mpm-*-graphlan_tree.txt tmp/unmatch_mpm.graphlan.xml
$graphlan_dir/home/wenchenaafc/graphlan/graphlan.py --format tmp/unmatch_mpm.graphlan.xml output_images/unmatch_mpm.graphlan.pdf

$graphlan_dir/graphlan_annotate.py --annot humann_profiling_unmatch/04b-*mpt*-graphlan_rings.txt humann_profiling_unmatch/04b-*-mpt-*-graphlan_tree.txt tmp/unmatch_mpt.graphlan.xml
$graphlan_dir/home/wenchenaafc/graphlan/graphlan.py --format tmp/unmatch_mpt.graphlan.xml output_images/unmatch_mpt.graphlan.pdf

## wheat
$graphlan_dir/graphlan_annotate.py --annot humann_profiling_match/04b-*mpm*-graphlan_rings.txt humann_profiling_match/04b-*-mpm-*-graphlan_tree.txt tmp/match_mpm.graphlan.xml
$graphlan_dir/home/wenchenaafc/graphlan/graphlan.py --format tmp/match_mpm.graphlan.xml output_images/match_mpm.graphlan.pdf

$graphlan_dir/graphlan_annotate.py --annot humann_profiling_match/04b-*mpt*-graphlan_rings.txt humann_profiling_match/04b-*-mpt-*-graphlan_tree.txt tmp/match_mpt.graphlan.xml
$graphlan_dir/graphlan.py --format pdf tmp/match_mpt.graphlan.xml output_images/match_mpt.graphlan.pdf

#########################################
## modify humann_profiling_unmatch/04b-hit-keg-mpt-cop-nul-nve-nve.txt to use graphlan
#########################################
#################
## microbiom (unmatched)
#################

#################
#pathway abundance
#################

## split Crop and treat
cut -f2- humann_profiling_unmatch/04b-hit-keg-mpt-cop-nul-nve-nve.txt | grep -e "^NAME" -e "Crop" -e "Treat" -e "^ko" | grep -v "^Crop_treat" |sed "/^NAME/ s/_untrimmed.fastq-hit-keg-mpt-cop-nul-nve-nve//g" > humann_profiling_unmatch/unmatch_04b-hit-keg-mpt-cop-nul-nve-nve.Crop.Treat.txt

$lefse_dir/format_input.py humann_profiling_unmatch/unmatch_04b-hit-keg-mpt-cop-nul-nve-nve.Crop.Treat.txt tmp/unmatch_04b-hit-keg-mpt-cop-nul-nve-nve.Crop.Treat.lefs -c 3 -s 2 -u 1 -o 1000000

# run the LEfSe biomarker discovery tool with default options apart for the 
# threshold on the LDA effect size which is increaset to 4

$lefse_dir/run_lefse.py tmp/unmatch_04b-hit-keg-mpt-cop-nul-nve-nve.Crop.Treat.lefs tmp/unmatch_04b-hit-keg-mpt-cop-nul-nve-nve.Crop.Treat.lefs.out -l 2 -y 1 -a 0.01 -w 0.01

# Plot the resulting list of biomarkers with the corresponsing effect size
# Crop, Treat, Sample
$lefse_dir/plot_res.py --format pdf tmp/unmatch_04b-hit-keg-mpt-cop-nul-nve-nve.Crop.Treat.lefs.out output_images/unmatch_04b-hit-keg-mpt-cop-nul-nve-nve.Crop.Treat.lefs_biomarkers.pdf

# Plot the biomarkers on the underlying cladogram
# Crop, Treat, Sample
$lefse_dir/plot_cladogram.py --format pdf tmp/unmatch_04b-hit-keg-mpt-cop-nul-nve-nve.Crop.Treat.lefs.out output_images/unmatch_04b-hit-keg-mpt-cop-nul-nve-nve.Crop.Treat.lefs_cladogram.pdf 

# Plot all biomarkers saving the images in one zip archive ("-f diff" is for plotting biomarkers only, with "-f all" one can plot all input features)
$lefse_dir/plot_features.py --format pdf -f diff --archive zip tmp/unmatch_04b-hit-keg-mpt-cop-nul-nve-nve.Crop.Treat.lefs tmp/unmatch_04b-hit-keg-mpt-cop-nul-nve-nve.Crop.Treat.lefs.out output_images/unmatch_04b-hit-keg-mpt-cop-nul-nve-nve.Crop.Treat_each_biomarker.zip

## combine Crop_treat
cut -f2- humann_profiling_unmatch/04b-hit-keg-mpt-cop-nul-nve-nve.txt | grep -e "^NAME" -e "Crop_treat" -e "^ko" |sed "/^NAME/ s/_untrimmed.fastq-hit-keg-mpt-cop-nul-nve-nve//g" > humann_profiling_unmatch/unmatch_04b-hit-keg-mpt-cop-nul-nve-nve.Crop_treat.txt

$lefse_dir/format_input.py humann_profiling_unmatch/unmatch_04b-hit-keg-mpt-cop-nul-nve-nve.Crop_treat.txt tmp/unmatch_04b-hit-keg-mpt-cop-nul-nve-nve.Crop_treat.lefs -c 2 -u 1 -o 1000000

# run the LEfSe biomarker discovery tool with default options apart for the 
# threshold on the LDA effect size which is increaset to 4

$lefse_dir/run_lefse.py tmp/unmatch_04b-hit-keg-mpt-cop-nul-nve-nve.Crop_treat.lefs tmp/unmatch_04b-hit-keg-mpt-cop-nul-nve-nve.Crop_treat.lefs.out -l 2 -y 1 -a 0.001 -w 0.001

# Plot the resulting list of biomarkers with the corresponsing effect size
# Crop, Treat, Sample
$lefse_dir/plot_res.py --format pdf tmp/unmatch_04b-hit-keg-mpt-cop-nul-nve-nve.Crop_treat.lefs.out output_images/unmatch_04b-hit-keg-mpt-cop-nul-nve-nve.Crop_treat.lefs_biomarkers.pdf

# Plot the biomarkers on the underlying cladogram
# Crop, Treat, Sample
$lefse_dir/plot_cladogram.py --format pdf tmp/unmatch_04b-hit-keg-mpt-cop-nul-nve-nve.Crop_treat.lefs.out output_images/unmatch_04b-hit-keg-mpt-cop-nul-nve-nve.Crop_treat.lefs_cladogram.pdf 

# Plot all biomarkers saving the images in one zip archive ("-f diff" is for plotting biomarkers only, with "-f all" one can plot all input features)
$lefse_dir/plot_features.py --format pdf -f diff --archive zip tmp/unmatch_04b-hit-keg-mpt-cop-nul-nve-nve.Crop_treat.lefs tmp/unmatch_04b-hit-keg-mpt-cop-nul-nve-nve.Crop_treat.lefs.out output_images/unmatch_04b-hit-keg-mpt-cop-nul-nve-nve.Crop_treat_each_biomarker.zip

#################
## wheat (matched)
#################

#################
#pathway abundance
#################

## split Crop and treat
cut -f2- humann_profiling_match/04b-hit-keg-mpt-cop-nul-nve-nve.txt | grep -e "^NAME" -e "Crop" -e "Treat" -e "^ko" | grep -v "^Crop_treat" |sed "/^NAME/ s/_trimmed.fastq-hit-keg-mpt-cop-nul-nve-nve//g" > humann_profiling_match/match_04b-hit-keg-mpt-cop-nul-nve-nve.Crop.Treat.txt

$lefse_dir/format_input.py humann_profiling_match/match_04b-hit-keg-mpt-cop-nul-nve-nve.Crop.Treat.txt tmp/match_04b-hit-keg-mpt-cop-nul-nve-nve.Crop.Treat.lefs -c 2 -s 3 -u 1 -o 1000000

# run the LEfSe biomarker discovery tool with default options apart for the 
# threshold on the LDA effect size which is increaset to 4

$lefse_dir/run_lefse.py tmp/match_04b-hit-keg-mpt-cop-nul-nve-nve.Crop.Treat.lefs tmp/match_04b-hit-keg-mpt-cop-nul-nve-nve.Crop.Treat.lefs.out -l 2 -y 1 -a 0.05 -w 0.05

# Plot the resulting list of biomarkers with the corresponsing effect size
# Crop, Treat, Sample
$lefse_dir/plot_res.py --format pdf tmp/match_04b-hit-keg-mpt-cop-nul-nve-nve.Crop.Treat.lefs.out output_images/match_04b-hit-keg-mpt-cop-nul-nve-nve.Crop.Treat.lefs_biomarkers.pdf

# Plot the biomarkers on the underlying cladogram
# Crop, Treat, Sample
$lefse_dir/plot_cladogram.py --format pdf tmp/match_04b-hit-keg-mpt-cop-nul-nve-nve.Crop.Treat.lefs.out output_images/match_04b-hit-keg-mpt-cop-nul-nve-nve.Crop.Treat.lefs_cladogram.pdf 

# Plot all biomarkers saving the images in one zip archive ("-f diff" is for plotting biomarkers only, with "-f all" one can plot all input features)
$lefse_dir/plot_features.py --format pdf -f diff --archive zip tmp/match_04b-hit-keg-mpt-cop-nul-nve-nve.Crop.Treat.lefs tmp/match_04b-hit-keg-mpt-cop-nul-nve-nve.Crop.Treat.lefs.out output_images/match_04b-hit-keg-mpt-cop-nul-nve-nve.Crop.Treat_each_biomarker.zip

## combine Crop_treat
cut -f2- humann_profiling_match/04b-hit-keg-mpt-cop-nul-nve-nve.txt | grep -e "^NAME" -e "Crop_treat" -e "^ko" |sed "/^NAME/ s/_trimmed.fastq-hit-keg-mpt-cop-nul-nve-nve//g" > humann_profiling_match/match_04b-hit-keg-mpt-cop-nul-nve-nve.Crop_treat.txt

$lefse_dir/format_input.py humann_profiling_match/match_04b-hit-keg-mpt-cop-nul-nve-nve.Crop_treat.txt tmp/match_04b-hit-keg-mpt-cop-nul-nve-nve.Crop_treat.lefs -c 2 -u 1 -o 1000000

# run the LEfSe biomarker discovery tool with default options apart for the 
# threshold on the LDA effect size which is increaset to 4

$lefse_dir/run_lefse.py tmp/match_04b-hit-keg-mpt-cop-nul-nve-nve.Crop_treat.lefs tmp/match_04b-hit-keg-mpt-cop-nul-nve-nve.Crop_treat.lefs.out -l 2 -y 1 -a 0.05 -w 0.05

# Plot the resulting list of biomarkers with the corresponsing effect size
# Crop, Treat, Sample
$lefse_dir/plot_res.py --format pdf tmp/match_04b-hit-keg-mpt-cop-nul-nve-nve.Crop_treat.lefs.out output_images/match_04b-hit-keg-mpt-cop-nul-nve-nve.Crop_treat.lefs_biomarkers.pdf

# Plot the biomarkers on the underlying cladogram
# Crop, Treat, Sample
$lefse_dir/plot_cladogram.py --format pdf tmp/match_04b-hit-keg-mpt-cop-nul-nve-nve.Crop_treat.lefs.out output_images/match_04b-hit-keg-mpt-cop-nul-nve-nve.Crop_treat.lefs_cladogram.pdf 

# Plot all biomarkers saving the images in one zip archive ("-f diff" is for plotting biomarkers only, with "-f all" one can plot all input features)
$lefse_dir/plot_features.py --format pdf -f diff --archive zip tmp/match_04b-hit-keg-mpt-cop-nul-nve-nve.Crop_treat.lefs tmp/match_04b-hit-keg-mpt-cop-nul-nve-nve.Crop_treat.lefs.out output_images/match_04b-hit-keg-mpt-cop-nul-nve-nve.Crop_treat_each_biomarker.zip



# use some picrust functions
cut -f1,3- humann_profiling_unmatch/04b-hit-keg-mpt-cop-nul-nve-nve.txt | sed "/^ID/ s/^/#OTU /g" | grep -e "^#" -e "^ko" | sed "1 i\# Constructed from biom file"| sed "/^#OTU ID/ s/_untrimmed.fastq-hit-keg-mpt-cop-nul-nve-nve//g" >tbd.1

biom convert -i tbd.1 -o tbd.biom
biom convert -i tbd.1 -o tbd.biom --to-json


set +x
