#/bin/sh
#!/usr/bash

# Start of quality trimming the raw reads from sample using Trimmomatic tool
echo " "
echo "-----------Quality trimming of raw reads using Trimmomatic tool starts now.....(processing)-------------"
export PATH=/isilon/biodiversity/users/prabr/hari/Trimmomatic-0.33/:$PATH
mkdir /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Trimmomatic_Output
mkdir /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Trimmomatic_Output/Logs
java -jar /isilon/biodiversity/users/prabr/hari/Trimmomatic-0.33/trimmomatic-0.33.jar PE -threads 20 -phred33 /isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/JSNsamples/S002994_r1.fastq /isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/JSNsamples/S002994_r2.fastq /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Trimmomatic_Output/output_forward_paired.fq.gz /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Trimmomatic_Output/output_forward_unpaired.fq.gz /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Trimmomatic_Output/output_reverse_paired.fq.gz /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Trimmomatic_Output/output_reverse_unpaired.fq.gz LEADING:25 TRAILING:25 SLIDINGWINDOW:4:15 MINLEN:36 > /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Trimmomatic_Output/Logs/Trimmomatic.logs 2>&1
echo " "
echo "-----------Quality trimming of raw reads using Trimmomatic tool successfully completed------------------"
# End of quality trimming the raw reads using Trimmomatic tool
