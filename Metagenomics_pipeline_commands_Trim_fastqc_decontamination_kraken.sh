#/bin/sh
#!/usr/bash

# Start of quality trimming the raw reads from sample using Trimmomatic tool
echo " "
echo"-----------Quality trimming of raw reads using Trimmomatic tool starts now.....(processing)-------------"
java -jar /isilon/biodiversity/users/prabr/hari/Trimmomatic-0.33/trimmomatic-0.33.jar PE -threads 20 -phred33 /isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/JSNsamples/S002994_r1.fastq /isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/JSNsamples/S002994_r2.fastq /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/output_forward_paired.fq.gz /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/output_forward_unpaired.fq.gz /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/output_reverse_paired.fq.gz /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/output_reverse_unpaired.fq.gz LEADING:25 TRAILING:25 SLIDINGWINDOW:4:15 MINLEN:36
echo " "
echo"-----------Quality trimming of raw reads using Trimmomatic tool successfully completed------------------"
# End of quality trimming the raw reads using Trimmomatic tool


# Start of quality check of trimmed reads from Trimmomatic analysis
echo " "
echo"-----------Quality check of trimmed reads from Trimmomatic analysis starts now....(processing)----------"
mkdir /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/FastQC_Report
cd /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/FastQC_Report
fastqc /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/output_forward_paired.fq.gz --outdir=/isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/FastQC_Report
fastqc /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/output_reverse_paired.fq.gz --outdir=/isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/FastQC_Report
echo " "
echo"-----------Quality check of trimmed reads from Trimmomatic analysis successfully completed--------------"
# End of quality check of trimmed reads from Trimmomatic analysis

# Start of decontamination step based on trimmed reads
echo " "
echo"-----------Decontamination of trimmed reads starts now......(processing)--------------------------------"
cd ..
export PATH=/isilon/biodiversity/users/prabr/hari/fastq_screen_v0.4.4/:$PATH
mkdir /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Fastq_Screen_Output
perl /isilon/biodiversity/users/prabr/hari/fastq_screen_v0.4.4/fastq_screen --threads 8 --nohits --aligner bowtie2 --bowtie2 "--local" --conf /isilon/biodiversity/users/prabr/hari/fastq_screen_v0.4.4/fastq_screen.conf --paired /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/output_forward_paired.fq.gz /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/output_reverse_paired.fq.gz --outdir /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Fastq_Screen_Output
echo ""
echo"------------Decontamination of trimmed reads successfully completed ------------------------------------"
# End of decontamination step based on trimmed reads

# Start of Assembling the trimmed reads using metAMOS pipeline
echo " "
echo"------------Assembling of trimmed reads process starts now.....(processing)-----------------------------"
gunzip /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Fastq_Screen_Output/output_forward_paired_no_hits_file.1.fastq.gz
gunzip /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Fastq_Screen_Output/output_reverse_paired_no_hits_file.2.fastq.gz
/isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/initPipeline -q -1 /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Fastq_Screen_Output/output_forward_paired_no_hits_file.1.fastq  /isilon/biodiversity/users/prabr/hari/
echo " "
echo"------------Assembling of trimmed reads process successfully completed----------------------------------"
# End of Assembling the trimmed reads using metAMOS pipeline

# Start of annotation step of re
gunzip /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Fastq_Screen_Output/output_*_paired.fq.gz
mdkir /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Kraken_Output/
echo " "
echo"------------Annotation of reads based on kraken database starts now....(processing)---------------------"
echo " "
export PATH=/isilon/biodiversity/users/prabr/hari/kraken/:$PATH
cd /isilon/biodiversity/users/prabr/hari/kraken/
kraken --paired --db /isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/Utilities/DB/kraken --quick --preload --min-hits 3 --threads 20 --output /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Kraken_Output/kraken_output.hits /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Fastq_Screen_Output/output_forward_paired_no_hits_file.1.fastq /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Fastq_Screen_Output/output_reverse_paired_no_hits_file.2.fastq
echo " "
echo"------------Annotation of reads based on kraken database successfully completed------------------------"
# End of annotation step of reads

# Start of translating the kraken output to taxonomical classification
echo " "
echo"------------Taxonomical classification process starts now.....(processing)-----------------------------"
export PATH =/isilon/biodiversity/users/prabr/hari/kraken/scripts/kraken-translate:$PATH
/isilon/biodiversity/users/prabr/hari/kraken/scripts/kraken-translate --db /isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/Utilities/DB/kraken/ /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Kraken_Output/kraken_output.hits > /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Kraken_Output/kraken_taxonomical_output.labels

echo " "
echo"------------Taxonomical classification process successfully completed----------------------------------"
# End of translating the kraken output to taxonomical classification


