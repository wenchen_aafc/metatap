#Jaiswamynarayan
my ($fasta_file_name,$copy_list_file_name,$contig_output_path,$sample_id) = @ARGV;
chomp($fasta_file_name);
chomp($copy_list_file_name);

unless(open(FASTA_IN,$fasta_file_name))
{
 print"Fasta file does not exist \n";
 exit();
}
unless(open(COPY_IN,$copy_list_file_name))
{
 print"Copy list reads not found\n";
 exit();
}

@fasta_content=<FASTA_IN>;
#print"$fasta_content[0]";
#print "$fasta_content[1]";
close FASTA_IN;
$i = 0;
%fasta_content_hash;
for($i=0;$i<$#fasta_content;$i=$i+2)
{
 $header = $fasta_content[$i];chomp($header);
 $sequence = $fasta_content[$i+1];
 #print "$header,$sequence\n";
 $fasta_content_hash{$header} = ([$header,$sequence]);
 #print"$fasta_content_hash{$header}[0]\n";
}

@copy_list_content= <COPY_IN>;
$output_path = "$contig_output_path"."$sample_id"."_"."Edited_Sample_Contigs.fasta";
open(OUT,">>$output_path");
$if_flag=0;
for($j=0;$j<=$#copy_list_content;$j++)
{
 $read_id_temp = $copy_list_content[$j]; 
 #print "$read_id\n";
 $read_id = ">".$read_id_temp;#print "$read_id,";
 chomp($read_id);
 $flag = $j;
 if(exists($fasta_content_hash{$read_id}))
 {
  print"Read found \n";$if_flag++;
  print "Read id - $read_id\n";
  #print @{$fasta_content_hash{$read_id}};
  print OUT "$fasta_content_hash{$read_id}[0]";
  print OUT "\n";
  print OUT "$fasta_content_hash{$read_id}[1]";
 }
}
print "\nflag : $flag\n";
print "If_flag : $if_flag\n";
close COPY_IN;
close OUT;
exit();
