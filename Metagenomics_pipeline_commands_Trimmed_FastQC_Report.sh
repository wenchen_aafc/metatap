# Start of quality check of trimmed reads from Trimmomatic analysis
echo " "
echo "-----------Quality check of trimmed reads from Trimmomatic analysis starts now....(processing)----------"
mkdir /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Trimmed_FastQC_Report
mkdir /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Trimmed_FastQC_Report/Logs
cd /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Trimmed_FastQC_Report
fastqc /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Trimmomatic_Output/output_forward_paired.fq.gz --outdir=/isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Trimmed_FastQC_Report > /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Trimmed_FastQC_Report/Logs/output_forward_paired.logs 2>&1
fastqc /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Trimmomatic_Output/output_reverse_paired.fq.gz --outdir=/isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Trimmed_FastQC_Report > /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Trimmed_FastQC_Report/Logs/output_reverse_paired.logs 2>&1
echo " "
echo "-----------Quality check of trimmed reads from Trimmomatic analysis successfully completed--------------"
# End of quality check of trimmed reads from Trimmomatic analysis
