#/bin/sh
#!/usr/bash
# Start of decontamination step based on assembled contigs from metAMOS assembler
echo " "
echo "-----------Step 5: Decontamination of assembled contigs starts now -------------------------------------------"
echo "Step 5 is processing .............."
mkdir /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Decontamination_of_Contigs/
mkdir /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Decontamination_of_Contigs/Logs
cut -f 1,2 /isilon/biodiversity/users/prabr/hari/nt_database_for_blast/S002994_contigs_BLAST_nt_Output_6_format > /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Decontamination_of_Contigs/S002994_contigs_BLAST_nt_Output_6_format_2columns.txt
export PATH=/isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/KronaTools/bin:$PATH 
perl /isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/KronaTools/Perl_Files_Ram_kraken/GetTaxIDFromGI.pl -a < /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Decontamination_of_Contigs/S002994_contigs_BLAST_nt_Output_6_format_2columns.txt > /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Decontamination_of_Contigs/S002994_Tax_ID_List.txt
export PATH=/isilon/biodiversity/users/prabr/hari/kraken/scripts/:$PATH
/isilon/biodiversity/users/prabr/hari/kraken/scripts/kraken-translate-Ram --mpa-format --db /isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/Utilities/DB/kraken/ /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Decontamination_of_Contigs/S002994_Tax_ID_List.txt > /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Decontamination_of_Contigs/S002994_Tax_ID_2_Lineage.txt
perl /isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/KronaTools/Perl_Files_Ram_kraken/BLAST_Decontamination_Removal.pl /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Decontamination_of_Contigs/S002994_Tax_ID_2_Lineage.txt /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Decontamination_of_Contigs/ k Viridiplantae
cut -f 1 /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Decontamination_of_Contigs/Contigs_Output_After_Decontamination.txt > /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Decontamination_of_Contigs/Final_Contig_List.txt
perl /isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/KronaTools/Perl_Files_Ram_kraken/Fastq_editor/Fasta_reader.pl /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/metAMOS_Output/proba.asm.contig.fa /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Decontamination_of_Contigs/Final_Contig_List.txt /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Decontamination_of_Contigs/ S002994

