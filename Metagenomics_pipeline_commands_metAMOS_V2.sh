#/bin/sh
#!/usr/bash

# Start of Assembling the trimmed reads using metAMOS pipeline
echo " "
echo "------------Assembling of trimmed reads process starts now.....(processing)-----------------------------"
#gunzip /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Fastq_Screen_Output/output_forward_paired_no_hits_file.1.fastq.gz
#gunzip /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Fastq_Screen_Output/output_reverse_paired_no_hits_file.2.fastq.gz
mkdir /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/metAMOS_Output/
mkdir /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/metAMOS_Output/Logs
cd /isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/metAMOS_Output/
export PATH=/isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/:$PATH
/isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/initPipeline -q -1 /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Fastq_Screen_Output/output_forward_paired_no_hits_file.1.fastq  /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Fastq_Screen_Output/output_reverse_paired_no_hits_file.2.fastq -i 30-150
/isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/runPipeline -q -a soap -p 15 -k 55 -f Preprocess,Assemble,MapReads,Propagate,Postprocess -n Annotate,FindORFs,FunctionalAnnotation,Classify,Abundance,FindScaffoldORFS,Scaffold 
#> /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/metAMOS_Output/Logs/metAMOS.logs 2>&1
cd ..
mv /isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/metAMOS_Output/P_2015*/Assemble/out/proba.asm.contig /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/metAMOS_Output/proba.asm.contig.fa
pwd
echo " "
echo "------------Assembling of trimmed reads process successfully completed----------------------------------"
# End of Assembling the trimmed reads using metAMOS pipeline

