#!/usr/bin/env perl

use strict;
use warnings;

use Bio::SeqIO;
use File::Basename;
use File::Copy;
#use File::spec;
use Scalar::Util qw/reftype/;
use Cwd;

my $profile=shift or die "merged profile?";
my @suffix=(".txt", ".tab", ".tabular");
my $abs_path = File::Spec->rel2abs($profile);
my $ext = (fileparse("$abs_path", qr/\.[^.]*/))[2];
my $dir= (fileparse("$abs_path", qr/\.[^.]*/))[1];
#print $abs_path, "\n", $dir, "\n\n", $ext, "\n";

my $basename = basename($abs_path, @suffix);
print $basename, "\t", "$basename.tab", "\n";

my $meta=shift or die "metadata in csv or tab format?";

my $h_meta={};
# watch out the encoding!
#open (my $fh, '<:utf8', "<$meta")  or die "Can't open $meta: $!";
open (my $fh, "<$meta")  or die "Can't open $meta: $!";

# skip to the header
my $samp;
my @treats;
my $treat
my $count=0;
while (<$fh>) {
  chomp $_;
  my @cols;
  if ($_=~ m/^sample/i) {
    @cols =split /[,\t]+/, $_;
    if ( scalar @cols >2 ) {
      print "More than 2 varialbes in the metadata\n\n";
      $samp=$cols[0];
      @treats=@cols[1..($meta_col-1)];

    } elsif ( scalar @cols =2 ) {
      $samp=$cols[0];
      $treat=$cols[($meta_col-1)];
    } else {
      print "no treatment? will use sampleID"
    }
    next;
  } else {
    @cols =split /[,\t]+/, $_;
    if (exists @treats) {
      foreach my $tr (@treats) {
      $h_meta->{$cols[0]}->{$tr}=;
    $h_meta->{$cols[0]}=\@cols[1..$meta_col-1];  
  }
}

print $samp, "\t", $treat, "\n";

close $fh;
foreach my $k (sort keys $h_meta) {
  print $k, "\t", $h_meta->{$k}, "\n";
}

open (F, "<$profile");
open (OUT, ">$dir/$basename.withMeta.$treat.tab");
my $count1=0;
while (<F>) {
  chomp $_;
  $count++;
  my @m;
  if ( /^ID\t/ ) {
    #print $_, "\n\n";
    #print OUT $_, "\n";
    my @cols = split /\t/, $_;
    @cols = map {$_ =~ s/\_.*//; $_} @cols;
    print join ("\t", @cols), "\n";
    push (@m, $treat);
    foreach my $s (@cols) {
      if (exists $h_meta->{$s}) {
        push (@m, $h_meta->{$s});
      }
    }
    print join ("\t", @m), "\n";
    print OUT join ("\t", @m), "\n";
  } else {
    print OUT $_, "\n";
  }
}

close F;
close OUT;


