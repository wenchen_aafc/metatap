#/bin/sh
#!/usr/bash
# export PATH=provide the location of trimmomatic:$PATH
export PATH=/isilon/biodiversity/users/prabr/hari/Trimmomatic-0.33/:$PATH
echo "Trimmomatic path has been set..."
java -jar Trimmomatic-0.33/trimmomatic-0.33.jar PE -threads 15 -phred33 /isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/JSNsamples/S002994_r1.fastq /isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/JSNsamples/S002994_r2.fastq /isilon/biodiversity/users/prabr/hari/Trimmomatic_Analysis/output_forward_paired.fq.gz /isilon/biodiversity/users/prabr/hari/Trimmomatic_Analysis/output_forward_unpaired.fq.gz /isilon/biodiversity/users/prabr/hari/Trimmomatic_Analysis/output_reverse_paired.fq.gz /isilon/biodiversity/users/prabr/hari/Trimmomatic_Analysis/output_reverse_unpaired.fq.gz LEADING:25 TRAILING:25 SLIDINGWINDOW:4:15 MINLEN:36
cd /isilon/biodiversity/users/prabr/hari/Trimmomatic_Analysis/
outputfiles=$(ls -l | wc -l)
echo $outputfiles
if [ $outputfiles = 4 ]
then 
echo "Successfully trimmed paired end reads"
echo "Two output files for forward and two output files for reverse reads are generated"
else
echo "Trimmomatic did not run properly"
fi
