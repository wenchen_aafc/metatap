#! /bin/bash

[ -f "log" ] && rm -fr "log";
exec >  >(tee -ai log)
exec 2> >(tee -ai log >&2)

set -x
#all tools: https://bitbucket.org/biobakery/biobakery/wiki/Home
#wget https://cran.r-project.org/src/contrib/infotheo_1.2.0.tar.gz
# sudo R CMD INSTALL --build infotheo_1.2.0.tar.gz 
#  sudo R CMD INSTALL --build ccrepe_1.1.1.tar.gz 
# for installing dependencies, check file shotgun_pipeline.sh

################################################
# index wheat genome (or other genomes for screening purposes
################################################
# cd ~/Desktop/Data/wheat_genome
# time bash ~/bowtie2-2.2.6/make_wheatgenome1to7ABD_index_use_fasta.sh

################################################
# check commands location
################################################
metaphlan_dir='/home/wenchenaafc/metaphlan'; 
[ ! -d "$metaphlan_dir" ] && echo "metaphlan direcotry doesn't exist" && exit 1;
metaphlan_script=$metaphlan_dir/metaphlan.py; 
[ ! -f "$metaphlan_script" ] && echo "path to metaphlan.py doesn't exist" && exit 1;
metaphlan_db=$metaphlan_dir/bowtie2db/mpa;
metaphlan_merge=$metaphlan_dir/utils/merge_metaphlan_tables.py;
[ ! -f "$metaphlan_merge" ] && echo "path to merge_metaphlan_tables.py doesn't exist" && exit 1;
lefse_dir='/home/wenchenaafc/lefse';
[ ! -d "$lefse_dir" ] && echo "lefse directory doesn't exist" && exit 1;
microbiome_helper_dir='/home/wenchenaafc/microbiome_helper';
[ ! -d "$microbiome_helper_dir" ] && echo "microbiome directory doesn't exist" && exit 1;
graphlan_dir='/home/wenchenaafc/graphlan';
[ ! -d "$graphlan_dir" ] && echo "graphlan direcotry doesn't exist" && exit 1;
humann_dir='/home/wenchenaafc/humann';
[ ! -d "$humann_dir" ] && echo "humann direcotry doesn't exist" && exit 1;

################################################
# setup input output and metadata locations
################################################
# tar xvf 140520_7001410_0133_BC4CPDACXX_lane4.tar | mv samples lane4;
# tar xvf 140520_7001410_0133_BC4CPDACXX_lane5.tar | mv samples lane5;

input_dir="/media/wenchenaafc/Data/140520_7001410_0133_BC4CPDACXX/input";
input_meta="/home/wenchenaafc/Data/140520_7001410_0133_BC4CPDACXX/meta.tab";
output_dir="/media/wenchenaafc/Data/140520_7001410_0133_BC4CPDACXX/output_trim_then_screen"

################################################
# check if input dir and metadata exists and empty,
################################################
echo $input_dir; [ ! -d $input_dir ] &&  echo "input directory $input_dir doesn't exist" && exit 1; [ -d $input_dir ] && [ "${input_dir:0:1}" != / ] && echo "input directory $input_dir should be absolute path" && input_dir=$(cd "$(dirname "$input_dir")"; pwd)/$(basename "$input_dir") || input_dir=$input_dir; echo $input_dir;

echo $input_meta; [ ! -f $input_meta ] &&  echo "metadata $input_meta doesn't exist" && exit 1; [ -f $input_meta ] && [ "${input_meta:0:1}" != / ] && echo "path to metadata file $input_meta should be absolute path" && input_meta=$(cd "$(dirname "$input_meta")"; pwd)/$(basename "$input_meta") || input_meta=$input_meta; echo $input_meta;

################################################
# check if output dir exists and empty,
################################################
#if [ -d $output_dir ]; then
    # the directory exists
#    [ "$(ls -A $output_dir)" ] && echo "output directory $output_dir is not Empty" && echo "will save output to $output_dir\_0" && mkdir $output_dir\_0 && output_dir=$output_dir\_0 && output_dir=$(cd "$(dirname "$output_dir")"; pwd)/$(basename "$output_dir") || echo "output directory $output_dir already exists but is empty"; output_dir=$output_dir && output_dir=$(cd "$(dirname "$output_dir")"; pwd)/$(basename "$output_dir"); 
#  else 
#    echo "output directory $output_dir doesn't exist" && mkdir $output_dir && output_dir=$(cd "$(dirname "$output_dir")"; pwd)/$(basename "$output_dir");
#fi

echo "input_dir is: $input_dir";
echo "metadata is: $input_meta";
echo "output_dir is: $output_dir";


################################################
# get sample list
################################################
[ -f "$input_dir/sample.list" ] && rm $input_dir/sample.list;
ls $input_dir/*fastq.gz|while read line; do echo $line; full=$(basename $line); base=${full%_[rR][1-2]*}; echo $full; echo $base >> temp; done; sort temp|uniq >> $input_dir/sample.list; rm temp; 


################################################
# Go to output folder
################################################
cd $output_dir;

################################################
## trim raw reads using Trimmomatic
################################################
#mkdir trimmed;
#for i in $(echo $(sed "s/\n/ /g" $input_dir/sample.list)); 
#do 
#  echo $i;
#  time java -jar ~/Trimmomatic-0.35/trimmomatic-0.35.jar PE -threads 20 -trimlog trimmed/$i.trimLog -phred33 $input_dir/$i\_[rR]1.fastq.gz $input_dir/$i\_[rR]2.fastq.gz trimmed/$i\_r1.paired.fastq.gz trimmed/$i\_r1.unpaired.fastq.gz trimmed/$i\_r2.paired.fastq.gz trimmed/$i\_r2.unpaired.fastq.gz LEADING:25 TRAILING:25 SLIDINGWINDOW:1:25 MINLEN:50 AVGQUAL:25;
  # real	170m4.330s (1.7G*2 S002984_r[1-2].fastq.gz)
  # user	31m58.951s
  # sys	34m18.322s

#done;

################################################
## screen unmatched sequence using bowtie
### run bowtie2 to screen out wheat seqs
################################################
mkdir screened_unmatched
for i in $(echo $(sed "s/\n/ /g" $input_dir/sample.list)); 
do 
  echo $i;
  #### write pairs that didn't align concordantly to genome
  time bowtie2 -x /home/wenchenaafc/Data/wheat_genome/wg1to7ABD -p 20 -1 trimmed/$i\_r1.paired.fastq.gz -2 trimmed/$i\_r2.paired.fastq.gz --un-conc-gz screened_unmatched/$i.fastq.gz >> /dev/null; mv screened_unmatched/$i.fastq.1.gz screened_unmatched/$i\_r1.paired.screen.fastq.gz; mv screened_unmatched/$i.fastq.2.gz screened_unmatched/$i\_r2.paired.screen.fastq.gz;
  #### write unpaired reads that didn't align to genome
  time bowtie2 -x /home/wenchenaafc/Data/wheat_genome/wg1to7ABD -p 20 --un-gz screened_unmatched/$i\_r1.unpaired.screen.fastq.gz -U <(zcat trimmed/$i\_r1.unpaired.fastq.gz) >> /dev/null;
  time bowtie2 -x /home/wenchenaafc/Data/wheat_genome/wg1to7ABD -p 20 --un-gz screened_unmatched/$i\_r2.unpaired.screen.fastq.gz -U <(zcat trimmed/$i\_r2.unpaired.fastq.gz) >> /dev/null;
done;

################################################
## screen matched sequence using bowtie
### run bowtie2 to screen sequences matched wheat seqs
################################################
mkdir screened_matched
for i in $(echo $(sed "s/\n/ /g" $input_dir/sample.list)); 
do 
  echo $i;
  #### write pairs that align concordantly to genome
  time bowtie2 -x /home/wenchenaafc/Data/wheat_genome/wg1to7ABD -p 20 -1 trimmed/$i\_r1.paired.fastq.gz -2 trimmed/$i\_r2.paired.fastq.gz --al-conc-gz screened_matched/$i.fastq.gz >> /dev/null; mv screened_matched/$i.fastq.1.gz screened_matched/$i\_r1.paired.screen.fastq.gz; mv screened_matched/$i.fastq.2.gz screened_matched/$i\_r2.paired.screen.fastq.gz;
  #### write unpaired reads that align to genome
  time bowtie2 -x /home/wenchenaafc/Data/wheat_genome/wg1to7ABD -p 20 --al-gz screened_matched/$i\_r1.unpaired.screen.fastq.gz -U <(zcat trimmed/$i\_r1.unpaired.fastq.gz) >> /dev/null;
  time bowtie2 -x /home/wenchenaafc/Data/wheat_genome/wg1to7ABD -p 20 --al-gz screened_matched/$i\_r2.unpaired.screen.fastq.gz -U <(zcat trimmed/$i\_r2.unpaired.fastq.gz) >> /dev/null;
done;


#sample="S00
#pair="r1 r2"; for i in $pair; do echo S002986_${i}.screen.fastq.bz2; bzip2 -dk S002986_${i}.screen.fastq.bz2; gzip -1kf S002986\_${i}.screen.fastq; done
#tar -cvjSf test.tar.bz2 *.screen.fastq.gz # 3.3G no advantage in saving space
#tar -pczf test.tar.gz *.screen.fastq.gz # 3.3G no advantage in saving space

################################################
## metaphlan
################################################

if [ -d "bowtie_out" ];
then 
  echo "bowtie_out directory already exists";
else 
  mkdir bowtie_out && echo "make bowtie_out directory";
fi;

if [ -d "profiled_samples" ];
then 
  echo "profiled_samples directory already exists"
else 
  mkdir profiled_samples && echo "make bowtie_out directory";
fi;

for i in $(echo $(sed "s/\n/ /g" $input_dir/sample.list)); 
do 
  echo $i;
  # non-local presets did not classify anything
  gunzip screened_unmatched/${i}\_[rR][1-2]*.*paired.screen.fastq.gz --stdout | $metaphlan_script --bowtie2db $metaphlan_dir/bowtie2db/mpa --bt2_ps sensitive --input_type multifastq --bowtie2out bowtie_out/${i}\_s.bt2out > profiled_samples/${i}\_s.txt
  gunzip screened_unmatched/${i}\_[rR][1-2]*.*paired.screen.fastq.gz --stdout | $metaphlan_script --bowtie2db $metaphlan_dir/bowtie2db/mpa --bt2_ps very-sensitive --input_type multifastq --bowtie2out bowtie_out/${i}\_vs.bt2out > profiled_samples/${i}\_vs.txt
  gunzip screened_unmatched/${i}\_[rR][1-2]*.*paired.screen.fastq.gz --stdout | $metaphlan_script --bowtie2db $metaphlan_dir/bowtie2db/mpa --bt2_ps sensitive-local --input_type multifastq --bowtie2out bowtie_out/${i}\_sl.bt2out > profiled_samples/${i}\_sl.txt
  gunzip screened_unmatched/${i}\_[rR][1-2]*.*paired.screen.fastq.gz --stdout | $metaphlan_script --bowtie2db $metaphlan_dir/bowtie2db/mpa --bt2_ps very-sensitive-local --input_type multifastq --bowtie2out bowtie_out/${i}\_vsl.bt2out > profiled_samples/${i}\_vsl.txt
done

################################################
## humann: function /pathway analyses
################################################
# make diamond db
# diamond makedb --in kegg.reduced.fasta -d kegg.reduced
# blastdb
#perl ~/microbiome_helper/run_pre_humann.pl -p 4 -o pre_humann/ screened/S00298C_r1.screen.fastq.gz
#diamond blastx -q screened/S002987_r1.screen.unpaired.fastq.gz --db ~/humann/data/diamond_db/kegg.reduced  -a tbd.faa -o tbd

## run blastx using diamond
[ -d "pre_humann_unmatch" ] && rm -fr "pre_humann_unmatch";
for i in $(echo $(sed "s/\n/ /g" $input_dir/sample.list)); 
do 
  echo $i;
  #time gunzip screened/${i}\_r*.screen.*.fastq.gz --stdout |$microbiome_helper_dir/run_pre_humann.pl -p 12 -o pre_humann/ $1
  time $microbiome_helper_dir/run_pre_humann.pl -p 20 -o pre_humann_unmatch/ screened_unmatched/${i}\_[rR][1-2]*.*paired.screen.fastq.gz
done;

[ -d "pre_humann_match" ] && rm -fr "pre_humann_match"
for i in $(echo $(sed "s/\n/ /g" $input_dir/sample.list)); 
do 
  echo $i;  
  time $microbiome_helper_dir/run_pre_humann.pl -p 20 -o pre_humann_match/ screened_matched/${i}\_[rR][1-2]*.*paired.screen.fastq.gz
done;

## merge blastx of each sample 
[ -d "pre_humann_match_merged" ] && rm -fr "pre_humann_match_merged"
mkdir pre_humann_match_merged;
for i in $(echo $(sed "s/\n/ /g" $input_dir/sample.list)); 
do 
  echo $i;
  cat pre_humann_match/${i}\_r*.screen.fastq.txt > pre_humann_match_merged/${i}\_matched.fastq.txt
done;

[ -d "pre_humann_unmatch_merged" ] && rm -fr "pre_humann_unmatch_merged"
mkdir pre_humann_unmatch_merged;
for i in $(echo $(sed "s/\n/ /g" $input_dir/sample.list)); 
do 
  echo $i;
  cat pre_humann_unmatch/${i}\_r*.screen.fastq.txt > pre_humann_unmatch_merged/${i}\_unmatched.fastq.txt
done;


###############################################################################################
## run scons
###############################################################################################
# make new directories
[ -d "humann_profiling_unmatch" ] && rm -fr "humann_profiling_unmatch"
[ -d "humann_profiling_match" ] && rm -fr "humann_profiling_match"
mkdir humann_profiling_unmatch;
mkdir humann_profiling_match;


###############################################################################################
# before run scons in humann, remove all files in the $humann_dir/input/ folder
###############################################################################################
if [ "$(ls -A $humann_dir/input)" ] 
then 
  echo "$humann_dir/input Not Empty";
  for i in $humann_dir/input/*; 
  do 
    if [ -f $i ] && [ ! -L $i ] 
    then 
      echo "$i exists and is not a symlink, remove $i" && rm $i; 
    else 
      echo "$i exists and is a symlink, unlink $i" && unlink $i; 
    fi;
  done;
fi; 

if [ "$(ls -A $humann_dir/output)" ] 
then 
  echo "$humann_dir/output Not Empty";
  for i in $humann_dir/output/*; 
  do 
    if [ -f $i ] && [ ! -L $i ] 
    then 
      echo "$i exists and is not a symlink, remove $i" && rm $i; 
    else 
      echo "$i exists and is a symlink, unlink $i" && unlink $i; 
    fi;
  done;
fi; 

###############################################################################################
# before run scons in humann, MUST put metadata in $humann_dir/input/ & $humann_dir/output/, and name the metadata in hmp_metadata.dat
###############################################################################################
if [ -f "$humann_dir/input/hmp_metadata.dat" ]
then 
  rm $humann_dir/input/hmp_metadata.dat
fi;

if [ -L "$humann_dir/input/hmp_metadata.dat" ] 
then
  unlink $humann_dir/input/hmp_metadata.dat
fi

cwd=$PWD
ln -s $PWD/pre_humann_unmatch_merged/*_unmatched.fastq.txt $humann_dir/input/
cd $humann_dir/
time scons
mv $humann_dir//output/*.txt $cwd/humann_profiling_unmatch/
unlink $humann_dir/input/hmp_metadata.dat
cd $cwd

###############################################################################################
# before run scons in humann, remove all files in the $humann_dir/input/ & $humann_dir/output/ folder
###############################################################################################
if [ "$(ls -A $humann_dir/input)" ] 
then 
  echo "$humann_dir/input Not Empty";
  for i in $humann_dir/input/*; 
  do 
    if [ -f $i ] && [ ! -L $i ] 
    then 
      echo "$i exists and is not a symlink, remove $i" && rm $i; 
    else 
      echo "$i exists and is a symlink, unlink $i" && unlink $i; 
    fi;
  done;
fi; 

if [ "$(ls -A $humann_dir/output)" ] 
then 
  echo "$humann_dir/output Not Empty";
  for i in $humann_dir/output/*; 
  do 
    if [ -f $i ] && [ ! -L $i ] 
    then 
      echo "$i exists and is not a symlink, remove $i" && rm $i; 
    else 
      echo "$i exists and is a symlink, unlink $i" && unlink $i; 
    fi;
  done;
fi; 

###############################################################################################
# before run scons in humann, MUST put metadata in $humann_dir/input/ , and name the metadata in hmp_metadata.dat
###############################################################################################
if [ -f "$humann_dir/input/hmp_metadata.dat" ]
then 
  rm $humann_dir/input/hmp_metadata.dat
fi;

if [ -L "$humann_dir/input/hmp_metadata.dat" ] 
then
  unlink $humann_dir/input/hmp_metadata.dat
fi

ln -s $input_meta $humann_dir/input/hmp_metadata.dat


ln -s $PWD/pre_humann_match_merged/*_matched.fastq.txt $humann_dir/input/

cd $humann_dir/
time scons
mv $humann_dir/output/*.txt $cwd/humann_profiling_match/

unlink $humann_dir/input/hmp_metadata.dat
cd $cwd


## output of humann
##interpret humann outputs
#04a...-mpt-... : pathway presence/absence
#04a...-mpm-...: module presence/absence
#04b...-mpt-... : pathway abundance
#04b...-mpm-... : module abundance



set +x
