#/bin/sh
#!/usr/bash
# Start of generating krona files for taxonomical classification
echo " "
echo "------------Generating krona files for taxonomical classification starts now...(processing)------------"
mkdir /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Krona_Output/
export PATH=/isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/KronaTools/bin:$PATH 
cd /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Krona_Output/
cp /isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/metAMOS_Output/P_2015*/Assemble/out/proba.contig.cnt /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/metAMOS_Output/proba.contig.cnt
perl /isilon/biodiversity/users/prabr/hari/metAMOS-1.5rc3/KronaTools/ImportKraken.pl -c -i -f /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/metAMOS_Output/proba.asm.contig.fa /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Kraken_Output/Kraken_Contig_Output.hits:/isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/metAMOS_Output/proba.contig.cnt:class
cd ..
cd ..
echo " "
echo "------------Generation of krona files for taxonomical classification successfully completed------------"

# End of generating krona files for taxonomical classification
