#/bin/sh
#!/usr/bash
# older version of fastq_screen
#export PATH=/isilon/biodiversity/users/prabr/hari/fastq_screen_v0.4.4/:$PATH
#mkdir /isilon/biodiversity/users/prabr/hari/Trimmomatic_Analysis/Fastq_Screen_Output
#perl /isilon/biodiversity/users/prabr/hari/fastq_screen_v0.4.4/fastq_screen --threads 8 --nohits --aligner bowtie2 --bowtie2 "--local" --conf /isilon/biodiversity/users/prabr/hari/fastq_screen_v0.4.4/fastq_screen.conf --paired /isilon/biodiversity/users/prabr/hari/Trimmomatic_Analysis/output_forward_paired.fq.gz /isilon/biodiversity/users/prabr/hari/Trimmomatic_Analysis/output_reverse_paired.fq.gz --outdir /isilon/biodiversity/users/prabr/hari/Trimmomatic_Analysis/Fastq_Screen_Output

#newer version of fastq_screen
# Start of decontamination step based on trimmed reads
echo " "
echo "-----------Decontamination of trimmed reads starts now......(processing)--------------------------------"
export PATH=/isilon/biodiversity/users/prabr/hari/fastq_screen_v0.4.4/:$PATH
mkdir /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Fastq_Screen_Output
mkdir /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Fastq_Screen_Output/Logs
perl /isilon/biodiversity/users/prabr/hari/fastq_screen_v0.4.4/fastq_screen --threads 8 --nohits --aligner bowtie2 --bowtie2 "--local" --conf /isilon/biodiversity/users/prabr/hari/fastq_screen_v0.4.4/fastq_screen.conf --paired /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Trimmomatic_Output/output_forward_paired.fq.gz /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Trimmomatic_Output/output_reverse_paired.fq.gz --outdir /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Fastq_Screen_Output > /isilon/biodiversity/users/prabr/hari/Metagenomics_Analysis/Fastq_Screen_Output/Logs/Fastq_Screen.logs 2>&1
echo ""
echo "------------Decontamination of trimmed reads successfully completed ------------------------------------"
# End of decontamination step based on trimmed reads
