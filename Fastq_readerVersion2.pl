#Jaiswamynarayan
print"Enter the fastq file name:";
$fastq_file_name=<STDIN>;
chomp($fastq_file_name);
unless(open(FASTQ_IN, $fastq_file_name))
{
 print"Fastq file does not exist \n";
 exit();
}
@fastq_content=<FASTQ_IN>;
#print "$fastq_content[2]";
close FASTQ_IN;
$i = 0;
%fastq_content_hash;
for($i=0;$i<$#fastq_content;$i=$i+4)
{
 $header = $fastq_content[$i];#chomp($header);
 $sequence = $fastq_content[$i+1];
 $separator = $fastq_content[$i+2];
 $score = $fastq_content[$i+3];
 #print "$header,$sequence,$separator,$score\n";
 $fastq_content_hash{$header} = ([$header,$sequence,$separator,$score]);
 print"$fastq_content_hash{$header}[0]\n";
}
print "Enter the list of reads file to be copied in fastq file:";
$delete_list_file_name = <STDIN>;
chomp($delete_list_file_name);
unless(open(DELETE_IN,$delete_list_file_name))
{
 print"Copy list reads not found\n";
 exit();
}
@delete_list_content= <DELETE_IN>;
#print "@delete_list_content";
open(OUT, ">>Edited_reads.fastq");
for($j=0;$j<=$#delete_list_content;$j++)
{
 $read_id = $delete_list_content[$j]; 
 
 #print "$read_id\n";
 if(exists ($fastq_content_hash{$read_id}))
 {
  print"Read found \n";
  print "Read id - $read_id\n";
  #print @{$fastq_content_hash{$read_id}};
  print OUT"$fastq_content_hash{$read_id}[0]";
  print OUT"$fastq_content_hash{$read_id}[1]";
  print OUT"$fastq_content_hash{$read_id}[2]";
  print OUT"$fastq_content_hash{$read_id}[3]";
}
}
exit();